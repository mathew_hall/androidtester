#!/usr/bin/env bash

TARGET="GS3"


if [ $TARGET == "GS3" ]; then
	APPUID=10147
else
	APPUID=10046 
fi

if [ "$(uname -s)" == "Darwin" ]; then
	echo "Using Mac Android tools"
	ASDK=/Users/mat/Library/AVD/sdk
	PATH=$PATH:$ASDK/platform-tools:$ASDK/tools
else
	echo "Using Linux Android tools"
	PATH=$PATH:/vagrant/android-sdk-linux/platform-tools:/vagrant/android-sdk-linux/tools
fi

{
	
	#Restore DB:
ssh mashriqi.dcs.shef.ac.uk /vagrant/reload_db.sh

	#Clear app state:
adb shell rm -r /data/data/org.wordpress.android

	#Restore app state:
adb shell cp -R /sdcard/org.wordpress.android /data/data/
adb shell chown $APPUID:$APPUID /data/data/org.wordpress.android
adb shell chown $APPUID:$APPUID '/data/data/org.wordpress.android/cache'
adb shell chown $APPUID:$APPUID '/data/data/org.wordpress.android/cache/*'
adb shell 'rm -r /data/data/org.wordpress.android/cache/*'

adb shell chown $APPUID:$APPUID /data/data/org.wordpress.android/lib

adb shell chown $APPUID:$APPUID /data/data/org.wordpress.android/databases
adb shell chown $APPUID:$APPUID '/data/data/org.wordpress.android/databases/*'
adb shell chown $APPUID:$APPUID /data/data/org.wordpress.android/shared_prefs
adb shell chown $APPUID:$APPUID '/data/data/org.wordpress.android/shared_prefs/*'

adb shell chmod -R 755 /data/data/org.wordpress.android

if [ $TARGET == "GS3" ]; then
#ONLY DO THESE ON GS3:
	#Disable CPU hotplugging
	adb shell "echo 1 > /sys/devices/system/cpu/cpufreq/pegasusq/max_cpu_lock"

	#Disable DVFS:
	adb shell "cat /sys/devices/system/cpu/cpu0/cpufreq/cpuinfo_max_freq > /sys/devices/system/cpu/cpu0/cpufreq/scaling_min_freq"

fi
	#Clear logcat buffer
adb logcat -c
	#Start app

#adb shell am start -W -a android.intent.action.MAIN org.wordpress.android/.ui.posts.PostsActivity
adb shell am instrument com.example.batteryburner.test/com.example.batteryburner.RemoteControlInstrumentation
sleep 5 #Note: sleep is needed because am -w will wait indefinitely for the instrumentation to return.

	#Clear AppScope Log


#if there's a lingering monkey it needs killing before we can carry on:
adb shell "ps" | grep com.android.commands.monkey 
if [ $? -eq "0" ]; then
	echo "Killing monkey"
	MONKEY_PID=$(adb shell "ps" | grep com.android.commands.monkey | awk '{print $2;}')
	adb shell "kill -9 $MONKEY_PID"
	#wait for monkey to die
	sleep 2
fi

#start a fresh monkey
adb shell "monkey -p org.wordpress.android --port 1080" &

adb shell cat /proc/appscope
# Write an entry in /var/log/apache2/access.log so we can see what requests each individual makes
curl mashriqi.dcs.shef.ac.uk/PREFLIGHT_START
sleep 2;
} >> /tmp/preflight.log