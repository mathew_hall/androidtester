#!/usr/bin/env bash

if [ "$(uname -s)" == "Darwin" ]; then
	echo "Using Mac Android tools"
	ASDK=/Users/mat/Library/AVD/sdk
	PATH=$PATH:$ASDK/platform-tools:$ASDK/tools
else
	echo "Using Linux Android tools"
	PATH=$PATH:/vagrant/android-sdk-linux/platform-tools:/vagrant/android-sdk-linux/tools
fi


#perl /Users/mat/Dropbox/Androtest/explore.pl logViews >> /tmp/viewLog

adb shell am force-stop org.wordpress.android
