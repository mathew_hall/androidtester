#!/usr/bin/env bash



if [ "$(uname -s)" == "Darwin" ]; then
	echo "Using Mac Android tools"
	ASDK=/Users/mat/Library/AVD/sdk/
	JAR_PATH=/Users/mat/Dropbox/AppScopeRemoteControl/dist
	CONFIG_PATH="/Users/mat/Documents/Vagrant VMs"
	ON_MAC=1
else
	echo "Using Linux Android tools"
	ASDK=/vagrant/android-sdk-linux
	JAR_PATH=/vagrant/dist
	CONFIG_PATH=/vagrant
fi



if [ ${SKIP-"0"} -ne "1" ]; then
echo "Start emulator"
/vagrant/start_emulator.sh &
echo "Waiting for device to come up"
sleep 300

fi



${ASDK}/platform-tools/adb wait-for-device
${ASDK}/platform-tools/adb forward tcp:4444 tcp:4444
${ASDK}/platform-tools/adb forward tcp:1080 tcp:1080
${ASDK}/platform-tools/adb shell monkey -p org.wordpress.android --port 1080 > /tmp/monkey.log&

java -cp  $JAR_PATH/AppScopeRemoteControl.jar:$JAR_PATH/lib ultrazord.search.SearchRunner @$CONFIG_PATH/configuration