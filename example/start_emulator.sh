#!/usr/bin/env bash
ASDK=/vagrant/android-sdk-linux

if [ -f /tmp/emulator.pid ]; then
	echo "Error: pid file exists!"
	exit
fi

WINDOW="${WINDOW--no-window}" #default to -no-window unless it's set in env


${ASDK}/tools/emulator -avd x86 -partition-size 300 $WINDOW -gpu off -qemu -enable-kvm  -boot-property dalvik.vm.execution-mode=int:portable &
echo $! > /tmp/emulator.pid

echo "Waiting for android to start"
${ASDK}/platform-tools/adb wait-for-device
echo "adb remount"
${ASDK}/platform-tools/adb remount
#${ASDK}/platform-tools/adb shell "echo '10.0.2.2	dummyserver' >> /system/etc/hosts"
${ASDK}/platform-tools/adb shell "echo '192.168.9.1	dummyserver' >> /system/etc/hosts"

${ASDK}/platform-tools/adb forward tcp:4444 tcp:4444
${ASDK}/platform-tools/adb forward tcp:1080 tcp:1080


while [ $(${ASDK}/platform-tools/adb shell getprop init.svc.bootanim | grep -c stopped) -ne "1" ]; do
	echo "Waiting a little longer..."
	sleep 30
done
sleep 10

echo "Unlocking device"
${ASDK}/platform-tools/adb shell input keyevent 82
echo "Device should be ready to go."

wait $(cat /tmp/emulator.pid)
echo "Cleanup pid file:"
rm /tmp/emulator.pid