#!/usr/bin/env bash
####
#
# This is the main script for Ultrazord:
# 	it manages running searches (environment setup)
#	runs the search
#	deals with the results afterwards.
#
# Configuration:
#	Host-specific details are automagically setup.
#	Search types can be changed by editing $types_to_run;
#	The config in (default) /vagrant/configuration.template specifies extra options beyonds search type.
#	NOTE: Template must not contain search_type param, it's added by this script
####


# Edit these for your target:

if [ "$(uname -s)" == "Darwin" ]; then
	echo "Using Mac Android tools"
	ASDK=/Users/mat/Library/AVD/sdk/
	JAR_PATH=/Users/mat/Dropbox/AppScopeRemoteControl/dist
	CONFIG_PATH="/Users/mat/Documents/Vagrant VMs"
	PATH=$PATH:$CONFIG_PATH
	SCRIPT_PREFIX="/Users/mat/Documents/Vagrant VMs/"
	ON_MAC=1
else
	echo "Using Linux Android tools"
	ASDK=/vagrant/android-sdk-linux
	JAR_PATH=/vagrant/dist
	CONFIG_PATH=/vagrant
	SCRIPT_PREFIX=/vagrant
	PATH=$PATH:/vagrant/android-sdk-linux/platform-tools:/vagrant
fi



echo "Copying scripts to /tmp..."

cp "$SCRIPT_PREFIX/wordpress_preflight.sh" /tmp
cp "$SCRIPT_PREFIX/wordpress_postflight.sh" /tmp

types_to_run=( ultrazord.search.GUIRandomSearch )

# Setup:

if [ "$(hostname)" = "mashriqi" ]; then
	CLASSES=/vagrant
	TEMPLATE=/vagrant/configuration.template
else
	CLASSES=/Users/mat/Dropbox/AppScopeRemoteControl
	TEMPLATE="/Users/mat/Documents/Vagrant VMs/configuration.template"
fi

if [ ! -d "output" ]; then
	echo "Making $(pwd)/output"
	mkdir output
fi

for search in "${types_to_run[@]}"
do

	DESTINATION_PATH="output/$search"

	if [ -d "$DESTINATION_PATH" ]; then
		echo "Getting rid of old $DESTINATION_PATH"
		rm -rf "$DESTINATION_PATH/"
	fi
	mkdir -p "$DESTINATION_PATH"

done

# Main loop:
ITERATIONS=1

for (( i=0; i<$ITERATIONS; i++)){
	for search in "${types_to_run[@]}"
	do
		echo -n "$(date) Start of iteration $i for search $search...		"
		
		#set up config file based on template and search type
		cat "$TEMPLATE" > /tmp/conf
		echo "-search_type" >> /tmp/conf
		echo "$search" >> /tmp/conf
		
		echo "-trace_log_path" >> /tmp/conf
		echo "output/$search/activity_traces_$i.txt" >> /tmp/conf
		
		
		#kill lingering app if there is one (Note: ultrazord takes care of preflighting.)
		wordpress_postflight.sh
	
		adb forward tcp:4444 tcp:4444
		adb forward tcp:1080 tcp:1080

		
		#set up destination
		
		DESTINATION_PATH="output/$search"

	
		#Run ultrazord:
		
		echo "! NOTE: Config file must be correctly set up with right paths to preflight and postflight!"
		
		java -jar $CLASSES/dist/AppScopeRemoteControl.jar @/tmp/conf 2>&1 | tee -a /tmp/all.log > $DESTINATION_PATH/ultrazord_$i.txt
		
		#Take a snapshot of the device's logcat
		adb logcat -d > $DESTINATION_PATH/adb_$i.txt
		#Take a copy of the search fitness log
		cp /tmp/status.log $DESTINATION_PATH/status_$i.txt
	
		#Pull screenshots out
		mkdir $DESTINATION_PATH/screenshots_$i
		mv /tmp/screenshot* $DESTINATION_PATH/screenshots_$i/
	
	
		echo "Done"
	done
}