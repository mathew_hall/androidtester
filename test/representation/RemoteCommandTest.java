/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package representation;

import com.example.batteryburner.tests.Commands;
import com.example.batteryburner.tests.Commands.UIInteraction;
import com.example.batteryburner.tests.Commands.UIInteraction.Builder;
import java.util.Locale;
import static junit.framework.Assert.assertEquals;
import static junit.framework.Assert.assertFalse;
import static junit.framework.Assert.assertTrue;
import junit.framework.TestCase;
import ultrazord.search.individuals.RemoteCommand;

/**
 *
 * @author mat
 */
public class RemoteCommandTest extends TestCase {

    public RemoteCommandTest(String testName) {
        super(testName);
    }

    @Override
    protected void setUp() throws Exception {
        super.setUp();
    }

    @Override
    protected void tearDown() throws Exception {
        super.tearDown();
    }
    // TODO add test methods here. The name must begin with 'test'. For example:
    // public void testHello() {}

    public void testEqualsForTwoSameCommand() {
        RemoteCommand a, b;
        a = RemoteCommand.press(50);
        b = new RemoteCommand();
        Builder build = Commands.UIInteraction.newBuilder();
        build.setType(UIInteraction.Type.PRESSVIEW);
        build.setId(50);
        b.setCommand(build.build());
        assertTrue(a.equals(b));
        assertEquals(a.hashCode(), b.hashCode());
    }

    public void testEqualsForTwoDifferentTypeCommand() {
        RemoteCommand a, b;
        a = RemoteCommand.press(50);
        b = new RemoteCommand();
        Builder build = Commands.UIInteraction.newBuilder();
        build.setType(UIInteraction.Type.PRESS);
        build.setId(50);
        b.setCommand(build.build());
        assertFalse(a.equals(b));
        assertFalse(a.hashCode() == b.hashCode());
    }

    public void testEqualsSameCommandDifferentIds() {
        RemoteCommand a, b;
        a = RemoteCommand.press(50);
        b = new RemoteCommand();
        Builder build = Commands.UIInteraction.newBuilder();
        build.setType(UIInteraction.Type.PRESSVIEW);
        build.setId(5);
        b.setCommand(build.build());
        assertFalse(a.equals(b));
        assertFalse(a.hashCode() == b.hashCode());
    }

    public void testEqualsForTwoSameCommandOneWithExtraInfo() {
        RemoteCommand a, b;
        a = RemoteCommand.press(50);
        b = new RemoteCommand();
        Builder build = Commands.UIInteraction.newBuilder();
        build.setType(UIInteraction.Type.PRESSVIEW);
        build.setId(50);
        build.setText("This should be igored");
        build.setItem(0xb00f);
        build.setKeycode(99);
        b.setCommand(build.build());
        assertTrue(a.equals(b));
        assertEquals(a.hashCode(), b.hashCode());
    }
}
