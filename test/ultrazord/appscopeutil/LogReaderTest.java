/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ultrazord.appscopeutil;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.List;
import java.util.Map;
import junit.framework.TestCase;

/**
 *
 * @author mat
 */
public class LogReaderTest extends TestCase {
    
    public void testExtractUidLogsReadsCorrectNumberOfLines() throws FileNotFoundException, IOException, IOException{
        LogReader lr = new LogReader(new File("testdata/logs/data"), 10147);
        
        List<Map<String,Object>> l = lr.extractUidLogs(new File("/Users/mat/Documents/AppScopeRemoteControl/testdata/logs/data/PHONE_ID/TIMESTAMP/power/power0.996.csv"));
        assertEquals(l.size(), 4);
                
    }
    
        public void testExtractUidLogsGetsCorrectTotal() throws FileNotFoundException, IOException, IOException{
        LogReader lr = new LogReader(new File("/Users/mat/Documents/AppScopeRemoteControl/testdata/logs/data"), 10147);
        
        Map<String, Double> logs  = lr.readLogs();
        assertEquals((int)(double)logs.get("cpu"), 27);
        
        
    }
}
