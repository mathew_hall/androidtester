/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ultrazord.search.arguments;

import java.util.Random;
import javax.security.auth.login.Configuration;
import junit.framework.TestCase;
import ultrazord.search.Config;

/**
 *
 * @author mat
 */
public class StringArgumentTest extends TestCase {
    
    public StringArgumentTest(String testName) {
        super(testName);
    }
    
    @Override
    protected void setUp() throws Exception {
        super.setUp();
        Config conf = new Config();
        conf.STRING_LENGTH = 61;
    }
    
    @Override
    protected void tearDown() throws Exception {
        super.tearDown();
    }


    public void testRandomString(){
        Random r = new Random();
        for(int i = 0; i < 100; i++){
            assertTrue(0 < StringArgument.randomString(r).length());
        }
        r = new Random(){

            @Override
            public int nextInt() {
                return 100;
            }
            
            @Override
            public int nextInt(int n){
                return Math.min(n-1, 100);
            }
            
        };
        assertEquals(61, StringArgument.randomString(r).length());
        
    }
}
