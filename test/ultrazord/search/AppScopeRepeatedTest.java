/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ultrazord.search;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;
import java.net.UnknownHostException;
import junit.framework.TestCase;
import ultrazord.output.OutputChannels;
import ultrazord.search.fitness.AllPowerFitnessFunction;
import ultrazord.search.fitness.FitnessEvaluator;
import ultrazord.search.fitness.UtimeFitnessFunction;
import ultrazord.search.individuals.RemoteCommand;
import ultrazord.search.individuals.RemoteControlProgram;

/**
 *
 * @author mat
 */
public class AppScopeRepeatedTest extends TestCase {

    public AppScopeRepeatedTest(String testName) {
        super(testName);
    }

    public void testRepeatedSameRunResults() throws FileNotFoundException, UnknownHostException, IOException, Exception {
        SearchRunner.init("-appscope_data_dir data -app_uid 10147 -preflight ../wordpress_preflight.sh -postflight ../wordpress_postflight.sh -forbidden_keycodes 26 -search_fitness_function ultrazord.search.fitness.AllPowerFitnessFunction".split(" "));
        RemoteControlProgram parent = new RemoteControlProgram();

        FitnessEvaluator fit = new AllPowerFitnessFunction();
        PrintWriter statusLog = new PrintWriter(OutputChannels.STATUS_LOG);

        int[] ids = {46, 15, 10, 12, 38, 32,32,4,6};

        for(int i : ids){
            parent.add(RemoteCommand.press(i));
        }

        statusLog.println("Time,Type,Category,Fitness");

        for (int i = 0; i < 100; i++) {
            double fitness = fit.getFitness(parent);
            //Search.logEntry(statusLog, "Fitness", "F", fitness + "");
            statusLog.flush();

        }


    }
}
