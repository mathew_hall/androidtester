/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ultrazord.search;

import com.example.batteryburner.tests.Commands;
import java.io.IOException;
import java.util.Random;
import junit.framework.TestCase;
import main.RemoteCommandFailureException;
import ultrazord.search.individuals.RemoteCommand;
import ultrazord.search.individuals.RemoteControlProgram;

/**
 *
 * @author mat
 */
public class GUIFullTraceSATest extends TestCase {


    /**
     * Test of getNumberOfEvaluations method, of class GUIFullTraceSA.
     */
    public void testGetNumberOfEvaluations() throws Exception {
        SearchRunner.init("-search_fitness_function DummyFitnessFunction".split(" "));
        GUIFullTraceSA search = new GUIFullTraceSA(){

            @Override
            public void sendCommand(RemoteCommand rc) throws IOException, RemoteCommandFailureException {
                
            }

            @Override
            public void delay() {
                
            }
            
            

            @Override
            public RemoteControlProgram getSuccessor(RemoteControlProgram seed, Random r, int length) throws IOException, RemoteCommandFailureException {
                RemoteControlProgram rp = new RemoteControlProgram();
                rp.setLength((int)(Math.random() * 5000));
                for(int i = 0; i < rp.getLength(); i++){
                    rp.add(new RemoteCommand().setCommand(Commands.UIInteraction.newBuilder().setType(Commands.UIInteraction.Type.PRESS).setId((int)(Math.random() * 50000)).buildPartial()));
                }
                return rp;
            }
            
            
            
        };
        
        
        search.runSearch();
        
        assertTrue("Test should start", true);
        
    }


}
