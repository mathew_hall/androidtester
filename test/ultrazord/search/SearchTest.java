/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ultrazord.search;

import ultrazord.search.individuals.MonkeyProgram;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.logging.Level;
import java.util.logging.Logger;
import junit.framework.TestCase;
import ultrazord.output.OutputChannels;
import ultrazord.search.fitness.AllPowerFitnessFunction;
import ultrazord.search.fitness.CPUFitnessFunction;
import ultrazord.search.fitness.AppScopeFitnessEvaluator;
import ultrazord.search.fitness.UtimeFitnessFunction;

/**
 *
 * @author mat
 */
public class SearchTest extends TestCase {
    
    public SearchTest(String testName) {
        super(testName);
    }
    
   /**
     * Test of init method, of class Search.
     */
    public void testInit() throws Exception {
        
        SearchRunner.init("-appscope_data_dir data -app_uid 10049 -preflight ../wordpress_preflight.sh -postflight ../wordpress_postflight.sh -forbidden_keycodes 26 -search_fitness_function ultrazord.search.fitness.UtimeFitnessFunction".split(" "));
        
        MonkeyProgram mp = new MonkeyProgram();
        mp.setLength(30);
        mp.randomise(new Random(1));
        UtimeFitnessFunction fit = new UtimeFitnessFunction();
                    PrintWriter statusLog = new PrintWriter(OutputChannels.STATUS_LOG);
        
        statusLog.println("Time,Type,Category,Fitness");
        for(int i =0; i < 100; i++){
            double fitness = fit.getFitness(mp);
            //logEntry(statusLog, "Fitness","F", fitness +"");
        }
        
          
       
    }
}
