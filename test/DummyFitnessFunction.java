
import java.io.IOException;
import java.net.UnknownHostException;
import ultrazord.search.fitness.FitnessEvaluator;
import ultrazord.search.individuals.Individual;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author mat
 */
public class DummyFitnessFunction extends FitnessEvaluator {

    public DummyFitnessFunction() throws UnknownHostException, IOException {
    }

    @Override
    protected double runTest(Individual script) throws Exception {
        return getFitness();
    }

    
    
    @Override
    protected double getFitness() throws Exception {
        return Math.random();
        
    }
    
}
