# Android GUI Explorer

This is a server-side application that generates input event sequences to a remote app listening on the network. It uses a protobuf protocol to send commands to an app started with instrumentation that runs in a separate thread and sends events via Robotium to the target.

This application depends on the protocol for the instrumentation, which is in a separate repository. The code can be copied or compiled and imported as a library to build this project.

The project also depends on classes from the Android SDK.

# Running

See the example in the example directory. A search requires two scripts: preflight and postflight. These initialise the instrumentation and clean up after a run. The configuration file causes them to run each time a fitness evaluation occurs. Fitness evaluations can be optionally repeated when measuring random values (e.g. processor time).

There are two paths of input injection: Robotium and Monkey. This is why the scripts run both the instrumentation and Monkey on the device.