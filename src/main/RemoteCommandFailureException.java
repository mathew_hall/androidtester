package main;

public class RemoteCommandFailureException extends Exception {
	public RemoteCommandFailureException(String message){
		super(message);
	}

}
