package main;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.Socket;
import java.net.UnknownHostException;

import com.beust.jcommander.JCommander;
import com.example.batteryburner.tests.Commands;
import com.example.batteryburner.tests.Commands.UIInteraction;
import com.example.batteryburner.tests.Commands.UIInteraction.Builder;
import com.example.batteryburner.tests.Commands.UIInteraction.Type;
import com.google.protobuf.CodedOutputStream;


public class Main {
	public static void main(String[] args) throws UnknownHostException, IOException{
		
		//Runtime.getRuntime().exec("adb forward tcp:4444 tcp:4444", envp)
		
		Options o = new Options();
		String[] argv = new String[args.length-1];
		System.arraycopy(args, 1, argv, 0, args.length-1);
		new JCommander(o, argv);
		
		Builder b = Commands.UIInteraction.newBuilder();
		
		b.setType(Type.valueOf(o.type));
		
		
		
		try{ b.setId(Integer.parseInt(o.id)); }catch(Exception e){
			 b.setId(Integer.parseInt(o.id,16));
		}
		if(o.text != null)
			b.setText(o.text);
		b.setItem(o.itemNumber);
		b.setKeycode(o.keycode);
		
		
		UIInteraction i = b.build();
		
		Socket clientSocket = new Socket(args[0],o.port);
		
		OutputStream s = clientSocket.getOutputStream();
		i.writeDelimitedTo(s);
		s.flush();
		BufferedReader is = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));
		
		//clientSocket.shutdownOutput();
		
		if(b.getType() == Type.GETACTIVITY || b.getType() == Type.GETVIEWS || b.getType() == Type.GETCLICKABLE || b.getType() == Type.GETITEMS || b.getType() == Type.GETENTRYLIST){
			String in;
			
			while((in = is.readLine()) != null){
				System.out.println(in);
			}
			
		}
		
		

		s.close();
		clientSocket.close();
	}
}
