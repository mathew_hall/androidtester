package main;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.io.IOException;
import java.io.PrintStream;
import java.io.PrintWriter;

import android.telephony.SignalStrength;

public class RandomWalk {

	private static final int STRING_LENGTH = 10;

	public static void randomWalk() throws IOException{
		RemoteApp app = new RemoteApp();
		RemoteApp monitor = new RemoteApp();
		Random r = new Random();
		
		while(app.getRecording().size()< 50){
			
			try{
				String currentActivity = monitor.getCurrentActivity();
				System.out.println(currentActivity);
			}catch(Exception e){
				System.out.println("Failed to get activity. Will retry in 5s");
				try{Thread.sleep(5000);}catch(Exception ex){}
				continue;
			}
		
			// escape local minima
			if(r.nextInt(100) == 1){
				try {
					System.out.print("Action=");
					app.pressKey(4);
				} catch (RemoteCommandFailureException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				continue;
			}
			
			List<String> views = null;
			try{
				views = monitor.getClickableViews();
				while(views.isEmpty()){
					try {
						Thread.sleep(2000);
					} catch (InterruptedException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					views  = monitor.getClickableViews();
					
				}
			}catch(Exception ex){
				ex.printStackTrace();
				try{Thread.sleep(5000);}catch(Exception iex){}
			}
			
			try{
				List<String> entryList = monitor.getViewList();
				ArrayList<Integer> textViews = new ArrayList<Integer>();
				
				int i = 0;
				for(String v : entryList){
					
					if(v.contains("T{")){
						textViews.add(i);
						i++;
					}
				}
				
				if(!textViews.isEmpty() && r.nextBoolean()){
					System.out.println("Filling text views");
					for(int tv : textViews){
						try {
							System.out.print("Action=");
							monitor.typeTextInField(tv, randomString(r));
						} catch (RemoteCommandFailureException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
							System.out.println("Failed to send text to button");
						}
					}
				}
			}catch(Exception exc){
				exc.printStackTrace();
			}
			int targetView = r.nextInt(views.size());
			
			boolean longPress = r.nextInt(100) < 33;
			
			try {
				System.out.print("Action=");
				app.pressViewByIndex(targetView, longPress);
			} catch (RemoteCommandFailureException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
				System.out.println("Failed to press target button");
			}
			
			//wait for the press to complete.
			//Note that this isn't perfect.
			//Ideally we should wait on the remote telling us the press completed.
			try {
				Thread.sleep(1000);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}
	
	
	public static void main(String[] args) throws IOException{
		Runtime.getRuntime().addShutdownHook(new Thread(){
			public void run(){
				postflight();
			}
		});
		
		for(int i =0; i < 100; i++){
			System.err.println("Running preflight");
			preflight();
			System.setOut(new PrintStream("/tmp/" + i + ".txt"));
			System.err.println("Starting random tests");
			randomWalk();
			System.err.println("Running postflight");
			postflight();
		}
		
	}

	private static void runScript(String script){
		try {
			Runtime.getRuntime().exec(script).waitFor();
		} catch (IOException | InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			System.exit(-1);
		}
	}
	
	public static void preflight(){
		runScript("/Users/mat/Dropbox/Androtest/wordpress_preflight.sh");
	}
	
	public static void postflight(){
		runScript("/Users/mat/Dropbox/Androtest/wordpress_postflight.sh");
	}
	
	private static String randomString(Random r) {
		String ret = "";
		int stringLength = 0;
		
		stringLength = r.nextInt(Math.max(1,STRING_LENGTH));
		
		for(int i =0; i < stringLength; i++){
			ret += (char)(r.nextInt('z'-'A') + 'A'); // gives a char in the range 'A' .. 'z' (includes a few non alpha chars between upper and lower sets);
		}
		return ret;
	}
}
