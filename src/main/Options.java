package main;
import com.beust.jcommander.Parameter;


public class Options {
	
	@Parameter(names = "-type")
	public int type;
	
	@Parameter(names= "-itemnumber")
	public int itemNumber;
	
	@Parameter(names= "-id")
	public String id ="";
	
	@Parameter(names= "-text")
	public String text = "";

	@Parameter(names ="-port")
	public int port = 4444;
	
	@Parameter(names ="-keycode")
	public int keycode = 0;
}
