package main;

import android.view.KeyEvent;
import com.example.batteryburner.tests.Commands;
import com.example.batteryburner.tests.Commands.UIInteraction;
import com.example.batteryburner.tests.Commands.UIInteraction.Builder;
import com.example.batteryburner.tests.Commands.UIInteraction.Type;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.lang.reflect.Field;
import java.net.Socket;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Represents the remote application, via the interface exposed by the RemoteControlInstrumentation component.
 * Each Protobuf command sent is logged from construction. This includes queries as well as modifiers.
 * It's recommended to use one RemoteApp for the state maintenance and one for the outputs. This way the recordings
 * are as noise-free as possible.
 * 
 * <b>Recording is automatic and starts from object construction</b>
 * @author mat
 *
 */
public class RemoteApp {
	
	private static final int TARGET_PORT = 4444;
	
	private List<UIInteraction> commandLog;
	
	public RemoteApp(){
		commandLog = new ArrayList<UIInteraction>();
	}

	public void pressViewById(int id) throws IOException, RemoteCommandFailureException{
		System.out.println("PRESS " + id);
		sendCommand(getBuilder().setType(Type.PRESS).setId(id));
	}
	
	public void typeTextInField(int id, String text) throws IOException, RemoteCommandFailureException{
		System.out.println("TYPE " + id + " Text: " + text);
		sendCommand(getBuilder().setType(Type.TEXT).setId(id).setText(text));
	}
	
	public void chooseSpinnerItem(int id, int itemNumber) throws IOException, RemoteCommandFailureException{
		System.out.println("CHOOSE " + id + " Item: " + itemNumber);
		sendCommand(getBuilder().setType(Type.SPINNER).setId(id).setItem(itemNumber));
	}
	
	public List<String> getViews() throws IOException, RemoteCommandFailureException{
		return sendCommand(getBuilder().setType(Type.GETVIEWS));
	}
	
	public String getCurrentActivity() throws IOException, RemoteCommandFailureException{
		List<String> res = sendCommand(getBuilder().setType(Type.GETACTIVITY));
		while(res.isEmpty()){
			try {
				Thread.sleep(500);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			res = sendCommand(getBuilder().setType(Type.GETACTIVITY));
		}
		return res.get(0);
		
	}
	
	public List<String> getClickableViews() throws IOException, RemoteCommandFailureException{
		return sendCommand(getBuilder().setType(Type.GETCLICKABLE));
	}
	
	public String getListItems(int number) throws IOException, RemoteCommandFailureException{
		return sendCommand(getBuilder().setType(Type.GETITEMS).setId(number)).get(0);
	}
	
	public void clickListItem(int number, int item) throws IOException, RemoteCommandFailureException{
		System.out.println("CLICK " + number + " Item: " + item);
		sendCommand(getBuilder().setType(Type.CLICKLISTITEM).setId(number).setItem(item));
	}
	
	public void pressKey(int keycode) throws IOException, RemoteCommandFailureException{
		System.out.println("PRESS " + keycode + " (" + keycode2Text(keycode) +")");
		sendCommand(getBuilder().setType(Type.SENDKEY).setKeycode(keycode));
	}
	
	private String keycode2Text(int keycode) {
		Class<KeyEvent> keycodeClass = KeyEvent.class;
		for(Field f : keycodeClass.getDeclaredFields()){
			if(f.getType() == Integer.TYPE){
				try {
					if(f.getInt(null) == keycode){
						return f.getName();
					}
				} catch (IllegalArgumentException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (IllegalAccessException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
		return null;
	}

	public List<String> getViewList() throws IOException, RemoteCommandFailureException {
		return sendCommand(getBuilder().setType(Type.GETENTRYLIST));
	}
	
	
	public void pressViewByIndex(int num, boolean longPress) throws IOException, RemoteCommandFailureException{
		System.out.println("PRESS " + num + (longPress?" LONG":""));
		sendCommand(getBuilder().setType(Type.PRESSVIEW).setId(num).setLongPress(longPress));
	}

	
	private static Builder getBuilder(){
		return Commands.UIInteraction.newBuilder();
	}
	
	public void clearRecording(){
		commandLog.clear();
	}
	public List<UIInteraction> getRecording(){
		return Collections.unmodifiableList(commandLog);
	}
	
	
	public ArrayList<String> sendCommand(Builder commandBuilder) throws IOException, RemoteCommandFailureException{
		Socket clientSocket = new Socket("localhost",TARGET_PORT);
		
		OutputStream s = clientSocket.getOutputStream();
		commandBuilder.build().writeDelimitedTo(s);
		commandLog.add(commandBuilder.build());
		s.flush();
		BufferedReader is = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));
		
		//clientSocket.shutdownOutput();
		
		ArrayList<String> lines = new ArrayList<String>();
		
		
		String in;
		boolean failure = false;
		while((in = is.readLine()) != null){
			if(failure){
				throw new RemoteCommandFailureException(in);
			}
			if(in.equals("FAILURE")){
				failure = true;
			}
			lines.add(in);
		}

		s.close();
		clientSocket.close();

		return lines;
	}
	

}
