/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ultrazord.output;

import java.io.IOException;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.io.Writer;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author mat
 */
public class MultiplePrintWriter extends Writer {
    
    private List<PrintWriter> writers;
    
    public MultiplePrintWriter(){
        writers = new ArrayList<>();
    }
    
    public MultiplePrintWriter(OutputStream[] os){
        this();
        for(OutputStream s : os) addOutStream(s);
    }
    
    public void addWriter(PrintWriter pw){
        writers.add(pw);
    }
    
    public void addOutStream(OutputStream os){
        writers.add(new PrintWriter(os));
    }
    
    @Override
    public void write(char[] cbuf, int off, int len) throws IOException {
        for(PrintWriter pw : writers){
            pw.write(cbuf, off, len);
        }
    }

    @Override
    public void flush() throws IOException {
        for(PrintWriter pw : writers){
            pw.flush();
        }
    }

    @Override
    public void close() throws IOException {
         for(PrintWriter pw : writers){
            pw.close();
        }
    }
    
}
