/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ultrazord.output;

import java.io.OutputStream;

/**
 *
 * @author mat
 */
public class OutputChannels {

    static {
        INDIVIDUAL_OUTPUT = new NullOutputStream();
        FITNESS_LOG = new NullOutputStream();
        STATUS_LOG = new NullOutputStream();
        CONFIGURATION = new NullOutputStream();
        TRACE_DATA = new NullOutputStream();
    }
    public static OutputStream INDIVIDUAL_OUTPUT;
    public static OutputStream FITNESS_LOG;
    public static OutputStream STATUS_LOG;
    public static OutputStream CONFIGURATION;
    public static OutputStream TRACE_DATA;
}
