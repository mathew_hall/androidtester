/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ultrazord.util;

import com.example.batteryburner.tests.Commands;
import com.example.batteryburner.tests.Commands.UIInteraction;
import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import ultrazord.search.Config;
import ultrazord.search.Search;
import ultrazord.search.SearchRunner;
import ultrazord.search.fitness.FitnessEvaluator;
import ultrazord.search.individuals.RemoteCommand;
import ultrazord.search.individuals.RemoteControlProgram;

/**
 *
 * @author mat
 */
public class IndividualRunner {
    public static void main(String[] args) throws FileNotFoundException, IOException, Exception{
        SearchRunner.init(args);
        Search.init();
        if(Config.configuration.INDIVIDUAL == null){
            System.exit(-1);
        }
        
        File f = new File(Config.configuration.INDIVIDUAL);
        
        
        FileInputStream fis = new FileInputStream(f);
        
        
        String buf = "";
        int in;
        while((in = fis.read()) != '\n'){
            buf += (char)in;
        }
        
        double fitness = Double.parseDouble(buf);
        
        Logger.getLogger(IndividualRunner.class.getName()).log(Level.INFO, "Recorded fitness is {0}", fitness);
        
        RemoteControlProgram commands = new RemoteControlProgram();
        
        boolean next = true;
        do{
            UIInteraction c = Commands.UIInteraction.parseDelimitedFrom(fis);
            if(c != null){
                commands.add(new RemoteCommand().setCommand(c));
            }else{
                next = false;
            }
        }while(next);
        
        Logger.getLogger(IndividualRunner.class.getName()).log(Level.INFO, "Read {0} commands", commands.size());
        
        FitnessEvaluator fit = Search.getFitnessFunctionInstance();
        
        for(int i = 0; i < 50; i++){
            double fvalue = fit.getFitness(commands);
            Logger.getLogger(IndividualRunner.class.getName()).log(Level.INFO, "Fitness: {0}", fvalue);
        
        }
        
    }
}
