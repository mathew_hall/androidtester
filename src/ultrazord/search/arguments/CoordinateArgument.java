/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ultrazord.search.arguments;

import java.util.Random;


/**
 *
 * @author mat
 */
public class CoordinateArgument extends Argument {
    private int x,y;

    @Override
    public void randomise(Random r) {
        x = r.nextInt(getConfiguration().SCREEN_WIDTH);
        y = r.nextInt(getConfiguration().SCREEN_HEIGHT);
    }

    @Override
    public void mutate(Random r) {
        int targets = r.nextInt(3);
        switch(targets){
            case 0:
                x = mutateCoordinate(x, r, getConfiguration().SCREEN_WIDTH);
            case 1:
                y = mutateCoordinate(y, r, getConfiguration().SCREEN_HEIGHT);
                break;
            case 2:
                x = mutateCoordinate(x, r, getConfiguration().SCREEN_WIDTH);
            
        }
    }
    
    private int mutateCoordinate(int coord, Random r, int max){
        return (coord + r.nextInt(max)) % max;
    }
    
    @Override
    public CoordinateArgument clone(){
        CoordinateArgument newMe = new CoordinateArgument();
        newMe.x = x;
        newMe.y = y;
        return newMe;
    }
    
    public String toString(){
        return x + " " + y;
    }
    
}
