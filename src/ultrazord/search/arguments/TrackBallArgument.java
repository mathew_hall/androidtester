/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ultrazord.search.arguments;

import java.util.Random;


/**
 *
 * @author mat
 */
public class TrackBallArgument extends Argument{
    
    private int x,y;

    @Override
    public void randomise(Random r) {
        x = r.nextInt(3) - 1; //nextInt returns 0,1,2; subtract 1 to put it in range -1,0,1
        y = r.nextInt(3) - 1;      
    }

    @Override
    public void mutate(Random r) {
        if(r.nextBoolean()){
            x = applyMutation(x,r);
        }else{
            y = applyMutation(y,r);
        }
    }
    
    private int applyMutation(int in, Random r){
        int sign = (r.nextBoolean())? 1 : -1;
        int shifted = in + 1; //put in range 0,1,2
        int amount = sign * r.nextInt(3);
        int ret = ((shifted + amount) % 3) -1;
        return ret;
    }

    @Override
    public TrackBallArgument clone() {
        TrackBallArgument newMe = new TrackBallArgument();
        newMe.x = x;
        newMe.y = y;
        return newMe;
    }
    
    public String toString(){
        return x + " " + y;
    }
}
