/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ultrazord.search.arguments;

import java.util.Random;

/**
 *
 * @author mat
 */
public class StringArgument extends Argument{
    
    private static char[] chars = new char[26 + 26 + 10];
    static {
        int i=0;
        for(char c = 'A'; c <= 'Z'; c++){
            chars[i] = c;
            i++;
        }
        for(char c = 'a'; c <= 'z'; c++){
            chars[i] = c;
            i++;
        }
        for(char c = '0'; c <= '9'; c++){
            chars[i] = c;
            i++;
        }
    }
    
    private String input;
    
    public static String randomString(Random r){
        StringBuilder b = new StringBuilder();
        int length = r.nextInt(getConfiguration().STRING_LENGTH) + 1;
        
       //all alphas + nums
        
        for(int i = 0; i < length; i++){
            b.append(chars[r.nextInt(chars.length)]);
        }
        
        return b.toString();
    }

    @Override
    public void randomise(Random r) {
        input = randomString(r);
    }

    @Override
    public void mutate(Random r) {
        randomise(r); //TODO: mutate this rather than randomising it
    }

    @Override
    public StringArgument clone() {
        StringArgument newMe = new StringArgument();
        newMe.input = input;
        return newMe;
    }
    
    public String toString(){
        return input;
    }
}
