/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ultrazord.search.arguments;

import java.util.Random;
import ultrazord.search.Config;

/**
 *
 * @author mat
 */
public abstract class Argument {
    protected static Config getConfiguration(){
        return Config.configuration;
    }
    
        public abstract void randomise(Random r);
        public abstract void mutate(Random r);
        
    @Override
        public abstract Argument clone();
    
    public Argument(){
        
    }
}
