/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ultrazord.search.arguments;

import java.util.Random;

/**
 *
 * @author mat
 */
public class KeyCodeArgument extends Argument{
    private int keycode;
    
    @Override
    public void randomise(Random r) {
        keycode = 1 + r.nextInt(getConfiguration().MAX_KEYCODE); //using max_keycode from ICS
        while(getConfiguration().FORBIDDEN_KEYCODES.contains(keycode)){
            keycode = 1 + r.nextInt(getConfiguration().MAX_KEYCODE); //generate new keycodes until one is accepted.
        }
    }

    @Override
    public void mutate(Random r) {
        int amt = r.nextInt(getConfiguration().MAX_KEYCODE);
        keycode += amt;
        keycode = keycode % getConfiguration().MAX_KEYCODE;
        while(getConfiguration().FORBIDDEN_KEYCODES.contains(keycode)){
            keycode = (keycode+1) % getConfiguration().MAX_KEYCODE;
        }
    }

    @Override
    public KeyCodeArgument clone() {
        KeyCodeArgument newMe = new KeyCodeArgument();
        newMe.keycode = keycode;
        return newMe;
    }
    
    public String toString(){
        return "" + keycode;
    }
    
}
