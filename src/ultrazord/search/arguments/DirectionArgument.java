/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ultrazord.search.arguments;

import java.util.Random;

/**
 *
 * @author mat
 */
public class DirectionArgument extends Argument{
    private Direction direction;

    @Override
    public void randomise(Random r) {
        int idx = r.nextInt(3);
        direction = Direction.values()[idx];
    }

    @Override
    public void mutate(Random r) {
        int idx = direction.ordinal();
        idx += r.nextInt(3);
        direction = Direction.values()[idx%3];
    }

    @Override
    public DirectionArgument clone() {
        DirectionArgument newMe = new DirectionArgument();
        newMe.direction = direction;
        return newMe;
    }
    
    public String toString(){
        return direction.toString();
    }
    
}

enum Direction{
    MOVE,
    UP,
    DOWN;
    
    public String toString(){
        return this.name().toLowerCase();
    }
    
}