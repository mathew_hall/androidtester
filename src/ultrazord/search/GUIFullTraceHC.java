/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ultrazord.search;

import java.io.IOException;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.logging.Level;
import java.util.logging.Logger;
import main.RemoteCommandFailureException;
import ultrazord.search.individuals.RemoteCommand;
import ultrazord.search.individuals.RemoteControlProgram;

/**
 *
 * @author mat
 */
public class GUIFullTraceHC extends GUISearch {
    @Override
    protected boolean isFitterThan(double fitness, double bestFitness) {
        return true; //Assume the first random move increases fitness, whether it does or not.
    }

    @Override
    public String getType() {
        return "Random GUI HC";
    }

    @Override
    public int getNumberOfEvaluations() {
        return 1;
    }
    


    @Override
    public Result runSearch() throws Exception {
        RemoteControlProgram parent = new RemoteControlProgram();
        parent.setLength(30);
        Random r = new Random();
        parent = getSuccessor(parent, r, Config.configuration.GUI_COMMAND_SEQUENCE_LENGTH);

        for (int i = 0; fit.getEvaluations() < Config.configuration.EVAL_LIMIT; i++) {
            RemoteControlProgram child;
            
            
            evaluate(parent);

            int point = r.nextInt(Config.configuration.GUI_COMMAND_SEQUENCE_LENGTH);
            child = parent.clone();
            
            
            
            //delete the tail:
            while(child.size() > point){
                child.remove(point);
            }
            
            Logger.getLogger(GUIFullTraceHC.class.getName()).log(Level.INFO, "Set cutpoint to {0}. Child now has {1} elements", new Object[]{point, child.size()});
            
            child = getSuccessor(child, r, Config.configuration.GUI_COMMAND_SEQUENCE_LENGTH);
            
            
            
            evaluate(child);
            
            logEntry(statusLog,"Fitness", "I", ""+child.getFitness());
            
            if(child.getFitness() > parent.getFitness()){
                parent = child;
                logEntry(statusLog,"Change", "F", ""+ parent.getFitness());
            }else{
                logEntry(statusLog,"NoChange", "I", "" + child.getFitness());
            }
            
        }

        double bestFitness = fit.getFitness(parent);
        logEntry(statusLog, "Fitness", "F", "" + bestFitness);
        return new Result(bestFitness, parent);
    }

    @Override
    public RemoteControlProgram getBestCommand(List<RemoteCommand> possibleCommands, RemoteControlProgram parent) throws Exception {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    

}
