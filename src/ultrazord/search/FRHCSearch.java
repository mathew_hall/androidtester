/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ultrazord.search;

import java.io.IOException;
import java.net.UnknownHostException;
import java.util.Random;
import static ultrazord.search.Search.init;

/**
 *
 * @author mat
 */
public class FRHCSearch extends HCSearch{
    private int RESTART_POINT = 5;
    
    public static void main(String... args) throws Exception {

        HCSearch search = new FRHCSearch();
        search.runSearch();
    }

    @Override
    protected boolean shouldRestart(int noIncrease, Random r) {
        return noIncrease >= RESTART_POINT;
    }

    @Override
    public String getType() {
        return "Random Restart RMHC";
    }

    
}
