/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ultrazord.search;

import com.example.batteryburner.tests.Commands;
import java.io.IOException;
import java.io.PrintWriter;
import java.lang.management.ManagementFactory;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.logging.Level;
import java.util.logging.Logger;
import main.RemoteApp;
import main.RemoteCommandFailureException;
import ultrazord.output.OutputChannels;
import static ultrazord.search.Search.preflight;
import ultrazord.search.individuals.Command;
import ultrazord.search.individuals.RemoteCommand;
import ultrazord.search.individuals.RemoteControlProgram;

/**
 *
 * @author mat
 */
public abstract class GUISearch extends Search<RemoteCommand> {
 protected Map<RemoteControlProgram, Double> fitnessCache;
 
 
    /**
     * Get a random string with length between 1 and 30.
     * @param r generator
     * @return random string of alphanumerics between 1 and 30 chars long.
     */
    protected static String randomString(Random r) {
        String ret = "";
        int stringLength = 0;
        stringLength = Math.max(1, r.nextInt(30));
        for (int i = 0; i < stringLength; i++) {
            ret += (char) (r.nextInt('z' - 'A') + 'A'); // gives a char in the range 'A' .. 'z' (includes a few non alpha chars between upper and lower sets);
        }
        return ret;
    }
    
    
    /**
     * Percentage (expressed as a decimal) that a fitness must be greater than
     * to be objectively superior. When this is 1, comparison is same as a > b
     * Use this parameter to adjust for the amount of variability caused by the
     * data, and balance it against the sample size.
     */
    protected double ERROR_UPPER_BOUND = 1.05;

    /**
     * Compare <code>fitness</code> to <code>bestFitness</code>.
     * This implementation includes <code>ERROR_UPPER_BOUND</code> to avoid
     * measurement errors. Replace the implementation to change criteria.
     * 
     * 
     * @param fitness individual's fitness
     * @param bestFitness fitness to beat
     * @return true if <code>fitness</code> is better than <code>bestFitness * 1.05</code>
     */
    protected boolean isFitterThan(double fitness, double bestFitness){
        return fitness >= bestFitness * ERROR_UPPER_BOUND;
    }
    public GUISearch(){
        fitnessCache = new HashMap<>();
    }
    
    /**
     * Send an input to the remote device.
     * <b>Note: ensure the device is ready to receive the event before sending it.</b>
     * Use preflight to ensure the remote instrumentation is running.
     * @param rc
     * @throws IOException
     * @throws RemoteCommandFailureException 
     */
    public void sendCommand(RemoteCommand rc) throws IOException, RemoteCommandFailureException {
        RemoteApp reader = new RemoteApp();
        reader.sendCommand(rc.getCommand().toBuilder());
    }

    /**
     * Query the remote device for a list of elements with which interaction is possible.
     * @param r generator, used to sort the returned list
     * @return a list of buttons and text fields that can be activated, plus an event for the back button.
     * @throws IOException
     * @throws RemoteCommandFailureException 
     */
    public List<RemoteCommand> getCommands(Random r) throws IOException, RemoteCommandFailureException {
        RemoteApp reader = new RemoteApp();
        ArrayList<RemoteCommand> commands = new ArrayList<>();
        List<String> views = reader.getViewList();
        int id = 0;
        for (String st : views) {
            if (st.contains("T{")) {
                commands.add(RemoteCommand.text(id, randomString(r)));
            }
            commands.add(RemoteCommand.press(id));
            id++;
        }
        commands.add(RemoteCommand.keyPress(4)); //back key
        return commands;
    }

    public void delay() {
        try {
            Thread.sleep(Config.configuration.REMOTE_BUTTON_DELAY);
        } catch (InterruptedException ex) {
            Logger.getLogger(GUIHCSearch.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Get a list of commands that can be supplied after sending each command in <code>p</code>
     * @param r random generator (used to shuffle the list)
     * @param p (possibly empty) sequence of commands to play before taking GUI snapshot
     * @return  a list of pressable Views + enabled text fields in the GUI
     */
    public List<RemoteCommand> getCommands(Random r, RemoteControlProgram p) {
        try {
            preflight();
            try {
                Thread.sleep(10000);
            } catch (InterruptedException ex) {
                Logger.getLogger(GUIHCSearch.class.getName()).log(Level.SEVERE, null, ex);
            }
            for (RemoteCommand rc : p) {
                sendCommand(rc);
                delay();
            }
            List<RemoteCommand> commands = getCommands(r);
            postflight();
            return commands;
        } catch (IOException ex) {
            Logger.getLogger(GUIHCSearch.class.getName()).log(Level.SEVERE, null, ex);
        } catch (RemoteCommandFailureException ex) {
            Logger.getLogger(GUIHCSearch.class.getName()).log(Level.SEVERE, null, ex);

            if (Config.configuration.WAIT_FOR_DEBUG) {
                Logger.getLogger(GUISearch.class.getName()).log(Level.INFO, "Waiting for debugger to attach; PID {0}", ManagementFactory.getRuntimeMXBean().getName());
                try {

                    Thread.sleep(600_000);
                } catch (Exception e) {
                }
            }

        }


        return null;
    }

    @Override
    public String getType() {
        return "GUIHCSearch";
    }

    @Override
    public Result runSearch() throws Exception {
        RemoteControlProgram parent = new RemoteControlProgram();
        parent.setLength(30);
        Random r = new Random();

        double bestFitness = 0; //fit.getFitness(parent); 

        for (int i = 0; i < Config.configuration.GUI_COMMAND_SEQUENCE_LENGTH; i++) {
            List<RemoteCommand> commands = getCommands(r, parent);
            Logger.getLogger(GUIHCSearch.class.getName()).log(Level.INFO, "{0} commands to evaluate for this iteration", commands.size());
            logEntry(statusLog, "Fitness", "F", "" + bestFitness);
            Collections.shuffle(commands, r);

            evaluate(parent);
            parent = getBestCommand(commands, parent);
            bestFitness = parent.getFitness();
            
            if(fit.getEvaluations() >= Config.configuration.EVAL_LIMIT){
                Logger.getLogger(GUISearch.class.getName()).log(Level.INFO, "Eval Limit hit {0}, fitness is {1}", new Object[]{fit.getEvaluations(), parent.getFitness()});
            }
            
        }

        return new Result(bestFitness, parent);
    }

    /**
     * An "evaluation" is a call to FF.evaluate(Individual).
     *
     * @return the number of fitness function evaluations used by the search.
     */
    public int getNumberOfEvaluations() {
        return fit.getEvaluations();
    }

    public abstract RemoteControlProgram getBestCommand(List<RemoteCommand> possibleCommands, RemoteControlProgram parent) throws Exception;

    /**
     * Get a random valid GUI sequence starting with <code>seed</code>.
     * @param seed a (possibly empty) of sequences to play before selecting a random element to append
     * @param r Random number generator
     * @param length the length of sequence to generate
     * @return a sequence of length <code>length</code>, prefix <code>seed</code>
     * @throws IOException if communication with the remote device failed after 10 retries.
     * @throws RemoteCommandFailureException if the remote device couldn't execute a command after 10 retries
     */
    public RemoteControlProgram getSuccessor(RemoteControlProgram seed, Random r, int length) throws IOException, RemoteCommandFailureException {

        for(int tries = 9; tries >= 0; tries--) {
            try {
                RemoteControlProgram newSequence = seed.clone();

                preflight();
                for (RemoteCommand rc : newSequence) {
                    sendCommand(rc);
                    delay();
                }
                while (newSequence.size() < length) {
                    List<RemoteCommand> commands = getCommands(r);
                    Collections.shuffle(commands, r);
                    assert commands.size() > 0;
                    sendCommand(commands.get(0));
                    newSequence.add(commands.get(0));
                    delay();
                }
                postflight();
                return newSequence;
            } catch (RemoteCommandFailureException | IOException rcfe) {
                Logger.getLogger(GUISearch.class.getName()).log(Level.INFO, "Exception when trying to get successors: {0} after {1} retries.", new Object[]{rcfe, tries});
                if(tries  == 0){
                    throw rcfe;
                }
            }
            }            
        return null; //Note: this is unreachable.
        }

    /**
     * Execute the program and get its fitness.
     * Fitness values are cached for instances of RemoteControlPrograms. If a cached value
     * is found, a costly evaluation won't be performed.
     * @param rp program to execute
     * @return fitness value
     * @throws Exception 
     */
    protected double evaluate(RemoteControlProgram rp) throws Exception {
        if (fitnessCache.containsKey(rp)) {
            Logger.getLogger(GUIFullTraceHC.class.getName()).log(Level.INFO, "Retrieved a cached fitness for an individual");
            rp.setFitness(fitnessCache.get(rp));
            return rp.getFitness();
        }
        double res = super.evaluate(rp);
        fitnessCache.put(rp, res);
        return res;
    }
    }
