/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ultrazord.search;

import ultrazord.search.individuals.MonkeyProgram;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.logging.Level;
import java.util.logging.Logger;
import ultrazord.output.OutputChannels;


/**
 *
 * @author mat
 */
public class RandomSearch extends Search {

    public static void main(String[] args) throws FileNotFoundException, Exception{

        
        RandomSearch rs = new RandomSearch();
        rs.runSearch();
    }
    
    @Override
    public Result runSearch() throws Exception {
        Random r = new Random();

        MonkeyProgram mp = new MonkeyProgram();
        
        mp.setLength(Config.configuration.INPUT_LENGTH);
        mp.randomise(r);
        
        double fitness = fit.getFitness(mp);

        logEntry(statusLog, "Start","F", fitness +"");
        Logger.getLogger(HCSearch.class.getName()).log(Level.INFO, "Fitness of start program: {0}", fitness);

        List<Result> best = new ArrayList<Result>();
        
        for(int i = 0; i < 500; i++){
            MonkeyProgram mutant = new MonkeyProgram();
            mutant.setLength(mp.getLength());
            mutant.randomise(r);
            
            double mutantF = fit.getFitness(mutant);
            if(mutantF > fitness){
                Logger.getLogger(HCSearch.class.getName()).log(Level.INFO, "Increase in fitness from {0} to {1}", new Object[]{(Object)new Double(fitness), (Object)new Double(mutantF)});
                mp = mutant;
                fitness = mutantF;
                logEntry(statusLog, "Change","F", fitness +"");
            }else{
                Logger.getLogger(HCSearch.class.getName()).log(Level.INFO, "Mutant didnt increase fitness from {0};  {1}. Mutants are {2} and {3} entries", new Object[]{(Object)new Double(fitness), (Object)new Double(mutantF), mp.size(), mutant.size()});
                logEntry(statusLog, "NoChange","I", mutantF +"");
                logEntry(statusLog, "Same", "F", fitness+ "");
            }

            if(i%10 == 0){
                Logger.getLogger(HCSearch.class.getName()).log(Level.INFO, "Done {0} iterations of 100", i);
            }
        }
        
        best.add(new Result(fitness, mp));
        
        Logger.getLogger(HCSearch.class.getName()).log(Level.INFO, "Finished");

        Result bestResult = best.get(0);
        
        for(Result m : best){
            if(m.fitness > bestResult.fitness){
                bestResult = m;
            }
        }
        
        return bestResult;
        
    }

    @Override
    public String getType() {
        return "Random";
    }
    
}
