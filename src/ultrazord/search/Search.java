/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ultrazord.search;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.logging.Level;
import java.util.logging.Logger;
import ultrazord.output.OutputChannels;
import ultrazord.search.fitness.FitnessEvaluator;
import ultrazord.search.individuals.Command;
import ultrazord.search.individuals.Individual;

/**
 *
 * @author mat
 */
public abstract class Search<T extends Command> {

    PrintWriter statusLog;

    public abstract Result runSearch() throws Exception;
    protected FitnessEvaluator fit;
    protected PrintWriter configLog;

    public Individual<T> getPrototype() {
        try {
            return Config.configuration.SEARCH_INDIVIDUAL.newInstance();
        } catch (Exception ex) {
            Logger.getLogger(Search.class.getName()).log(Level.SEVERE, null, ex);
            System.exit(-1);
        }
        return null;
    }

    public static FitnessEvaluator getFitnessFunctionInstance() {
        try {
            return Config.configuration.SEARCH_FITNESS_FUNCTION.newInstance();
        } catch (Exception ex) {
            Logger.getLogger(Search.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    public Search() {

        try {
            init();
            fit = getFitnessFunctionInstance();
            configLog = new PrintWriter(OutputChannels.CONFIGURATION);
            configLog.println("Type=" + getType());
            configLog.flush();

            statusLog = new PrintWriter(OutputChannels.STATUS_LOG);
            statusLog.println("Time,Type,Category,Fitness,Evaluations");
        } catch (Exception ex) {
            Logger.getLogger(Search.class.getName()).log(Level.SEVERE, null, ex);
            System.exit(-1);
        }
    }

    private static void runScript(String script) {
        try {
            Runtime.getRuntime().exec(script).waitFor();
        } catch (IOException | InterruptedException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            System.exit(-1);
        }
    }

    public static void preflight() {
        runScript(Config.configuration.PREFLIGHT_SCRIPT);
    }

    public static void postflight() {
        runScript(Config.configuration.POSTFLIGHT_SCRIPT);
    }

    public static void init() throws FileNotFoundException {


        Runtime.getRuntime().addShutdownHook(new Thread() {
            public void run() {
                postflight();
            }
        });

        OutputChannels.INDIVIDUAL_OUTPUT = new FileOutputStream("/tmp/individuals.log");
        OutputChannels.FITNESS_LOG = new FileOutputStream("/tmp/fitness.log");
        OutputChannels.STATUS_LOG = new FileOutputStream("/tmp/status.log");
        OutputChannels.CONFIGURATION = new FileOutputStream("/tmp/configuration.txt");
        OutputChannels.TRACE_DATA = Config.configuration.TRACE_LOG_PATH;

        PrintWriter configLog = new PrintWriter(OutputChannels.CONFIGURATION);
        configLog.println(Config.configuration.toString());

        PrintWriter pw = new PrintWriter(OutputChannels.TRACE_DATA, true);

        pw.println("types");
        pw.println("#Note these are subject to change; not sure if 'return' values should be here");
        pw.println("#or if having variables with same names between events is OK");
        pw.println("pressview button_id:N activity_from:S activity_to:S");
        pw.println("text field_id:N string:S activity_from:S activity_to:S");
        pw.println("backbutton");
        pw.flush();


    }

    public void logEntry(PrintWriter statusLog, String type, String cat, String arg) {
        statusLog.append(System.currentTimeMillis() + "");
        statusLog.append(",");
        statusLog.append(type);
        statusLog.append(",");
        statusLog.append(cat);
        statusLog.append(",");
        statusLog.append(arg);
        statusLog.append(",");
        statusLog.append(fit.getEvaluations() + "");
        statusLog.append("\n");
        statusLog.flush();
    }

    public abstract String getType();

    static class Result {

        double fitness;
        Individual individual;

        public Result(double fitness, Individual i) {
            this.fitness = fitness;
            this.individual = i;
        }
    }

    public double evaluate(Individual<T> i) throws Exception {
        if (i.isEvaluated()) {
            return i.getFitness();
        }
        double fitness = fit.getFitness(i);
        i.setFitness(fitness);
        return fitness;
    }
}
