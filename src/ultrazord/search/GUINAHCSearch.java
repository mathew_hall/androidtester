/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ultrazord.search;

/**
 *
 * @author mat
 */
public class GUINAHCSearch extends GUIHCSearch {

    @Override
    /**
     * We only advance if there's been an increase (the old parent fitness is
     * less than the best we've seen). Note that the isFitterThan function
     * defines what means "best", so this condition essentially means, "has any
     * individual resulted in isFitterThan returning true?". Even though the
     * test is !=, because isFitterThan sets it, the condition still works.
     */
    protected boolean shouldAdvanceSearch(double parentFitness, double bestFitness, int remainingCommands) {
        return bestFitness != parentFitness;
    }
}
