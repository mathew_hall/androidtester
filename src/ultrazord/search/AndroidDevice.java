/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ultrazord.search;

import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.jfree.util.Configuration;

/**
 *
 * @author mat
 */
public class AndroidDevice {
    
    public static void runScript(String script) throws IOException, InterruptedException{
        Process p = Runtime.getRuntime().exec(script);
        p.waitFor();
    }
    
    public static void postflight() throws IOException, InterruptedException{
        if(Config.configuration.POSTFLIGHT_SCRIPT != null){
            Logger.getLogger(AndroidDevice.class.getName()).log(Level.INFO, "Running postflight script {0}", (Object)Config.configuration.POSTFLIGHT_SCRIPT);
            runScript(Config.configuration.POSTFLIGHT_SCRIPT);
        }
    }
    
    public static void preflight() throws IOException, InterruptedException{
        if(Config.configuration.PREFLIGHT_SCRIPT != null){
            Logger.getLogger(AndroidDevice.class.getName()).log(Level.INFO, "Running preflight script {0}", (Object)Config.configuration.PREFLIGHT_SCRIPT);
            runScript(Config.configuration.PREFLIGHT_SCRIPT);
        }
    }
}
