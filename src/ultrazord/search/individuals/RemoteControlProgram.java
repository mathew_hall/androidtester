/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ultrazord.search.individuals;

import com.example.batteryburner.tests.Commands;
import java.io.IOException;
import java.io.PrintStream;
import java.security.KeyStore;
import ultrazord.search.fitness.IndividualVisitor;

/**
 *
 * @author mat
 */
public class RemoteControlProgram extends Individual <RemoteCommand>{

    @Override
    public Individual getInstance() {
        return new RemoteControlProgram();
    }

    @Override
    public RemoteCommand getCommandInstance() {
        return new RemoteCommand();
    }

    @Override
    public void accept(IndividualVisitor iv) {
        iv.visit(this);
    }
    
    public void writeToFile(PrintStream out) throws IOException{
        for(RemoteCommand  rc : this){
            rc.getCommand().writeDelimitedTo(out);
        }
        out.flush();
    }

    @Override
    public RemoteCommand remove(int index){
        isEvaluated = false;
        fitness = 0;
        return super.remove(index);
    }
    
    public RemoteControlProgram clone(){
        RemoteControlProgram newMe = new RemoteControlProgram();
        for(RemoteCommand rc : this){
            newMe.add(rc);
        }
        
        if(isEvaluated()){
            newMe.setFitness(getFitness());
        }
        
        if(this.sealed){
            newMe.seal();
        }
        
        
        return newMe;
    }

}
