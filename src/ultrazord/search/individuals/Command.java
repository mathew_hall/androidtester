/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ultrazord.search.individuals;

import java.util.Random;

/**
 *
 * @author mat
 */
public abstract class Command {

    public abstract Command clone();

    public abstract void mutate(Random r);

    public abstract void randomise(Random r);

    public abstract String toString();
    
}
