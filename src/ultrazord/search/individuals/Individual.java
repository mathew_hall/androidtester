/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ultrazord.search.individuals;

import java.io.IOException;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.Random;
import java.util.logging.Level;
import java.util.logging.Logger;
import ultrazord.search.fitness.IndividualVisitor;

/**
 *
 * @author mat
 */
public abstract class Individual<T extends Command> extends ArrayList<T> {
    static final int MIN_LENGTH = 5;
    double fitness;
    protected boolean isEvaluated;
    int length;
    boolean sealed = false;

    public void clearEvaluated() {
        isEvaluated = false;
    }

    
    public abstract Individual<T> getInstance();
    public abstract T getCommandInstance();
    public Command[] getCommands(){
                return toArray(new Command[]{});
    }
    
    @Override
    public Individual<T> clone() {
        Individual<T> newMe = getInstance();
        newMe.length = length;
        newMe.isEvaluated = isEvaluated;
        newMe.fitness = fitness;
        for (int i = 0; i < size(); i++) {
            newMe.add((T)get(i).clone());
        }
        return newMe;
    }

    public double getFitness() {
        return fitness;
    }

    public int getLength() {
        return length;
    }

    public boolean isEvaluated() {
        return isEvaluated;
    }
    
    public boolean add(T entry){
        if(sealed){
            return false;
        }
        isEvaluated = false;
        return super.add(entry);
    }

    public void mutate(Random r) {
        if (sealed) {
            return;
        }
        fitness = 0;
        isEvaluated = false;
        int num = 1; //r.nextInt(size());
        Logger.getLogger(MonkeyProgram.class.getName()).log(Level.INFO, "Mutating {0} elements", num);
        for (int i = 0; i < num; i++) {
            get(r.nextInt(size())).mutate(r);
        }
    }

    public void randomise(Random r) {
        //        int size = r.nextInt(length-MIN_LENGTH) + MIN_LENGTH;
        if (sealed) {
            return;
        }
        fitness = 0;
        isEvaluated = false;
        int size = length;
        clear();
        for (int i = 0; i < size; i++) {
            T me = getCommandInstance();
            me.randomise(r);
            add(me);
        }
    }

    public Individual<T> seal() {
        sealed = true;
        return this;
    }

    public void setFitness(double fitness) {
        isEvaluated = true;
        this.fitness = fitness;
    }

    public void setLength(int length) {
        this.length = length;
    }

    public String toString() {
        StringBuilder sb = new StringBuilder();
        for (Command me : this) {
            sb.append(me);
            sb.append("\n");
        }
        return sb.toString();
    }
    
    public abstract void writeToFile(PrintStream out) throws IOException;
    public abstract void accept(IndividualVisitor iv);
    
}
