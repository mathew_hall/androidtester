/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ultrazord.search.individuals;

import com.example.batteryburner.tests.Commands;

import java.util.Random;

/**
 *
 * @author mat
 */
public class RemoteCommand extends Command{
   
    
    
    
    private Commands.UIInteraction cmd;
    
    public Commands.UIInteraction getCommand(){
        return cmd;
    }
    
    //private int id; //TODO: figure out how to scale these into sane bounds - indexes into known IDs?? Not used as-is anyway.
    
    public RemoteCommand setCommand(Commands.UIInteraction c){
        this.cmd = c;
        return this;
    }
    
    public RemoteCommand setNumber(int n){
        cmd = cmd.toBuilder().setId(n).build();
        
        return this;
    }
    
    public RemoteCommand setText(String arg){
        cmd = cmd.toBuilder().setText(arg).build();
        
        return this;
    }
    
    public RemoteCommand setSpinnerItem(int num){
        cmd = cmd.toBuilder().setItem(num).build();
        return this;
    }
    
 

    @Override
    public Command clone() {
        RemoteCommand newMe = new RemoteCommand();
        newMe.setCommand(cmd.toBuilder().build());
        return newMe;
    }
    
    private static RemoteCommand newInstance(Commands.UIInteraction.Type type, int id){
         RemoteCommand ret = new RemoteCommand();
         ret.cmd =  Commands.UIInteraction.newBuilder().setType(type).setId(id).buildPartial();
         return ret;
    }
    
    public static RemoteCommand press(int id){
       return newInstance(Commands.UIInteraction.Type.PRESSVIEW, id);
    }
    
    public static RemoteCommand text(int id, String str){
        return newInstance(Commands.UIInteraction.Type.TEXT, id).setText(str);
    }
    
    public static RemoteCommand keyPress(int keycode){
        RemoteCommand rc = newInstance(Commands.UIInteraction.Type.SENDKEY, keycode);
        rc.setCommand(rc.cmd.toBuilder().setKeycode(keycode).build());
        return rc;
    }

    @Override
    public void mutate(Random r) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void randomise(Random r) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public String toString() {
        return cmd.toString();
    }
    
    @Override
    public int hashCode(){
        int hashcode = 1;
        hashcode = 31 * hashcode + cmd.getType().ordinal();
        switch(cmd.getType()){
            case CLICKLISTITEM:
                hashcode = 31 * hashcode + (int)cmd.getItemId();
            
            case TEXT:
                hashcode = 31 * hashcode + cmd.getText().hashCode();
                
            case PRESS:
            case PRESSVIEW:
                hashcode = 31 * hashcode + cmd.getId();
                break;
            case SENDKEY:
                hashcode = 31 * hashcode + cmd.getKeycode();
                
            default:
        }
        
        return hashcode;
    }

    @Override
    public boolean equals(Object obj) {
        if(!(obj instanceof RemoteCommand)){
            return false;
        }
        Commands.UIInteraction c = cmd, o = ((RemoteCommand)obj).cmd;
        
        if(c.getType() != o.getType()){
            return false;
        }
        
        switch(o.getType()){
            case CLICKLISTITEM:
                if(o.getItem() != c.getItem())
                    return false;

            case TEXT:
                //TODO: should text be checked?
                
            case PRESS:
            case PRESSVIEW:
                if(o.getId() != c.getId()){
                    return false;
                }
                break;
            case SENDKEY:
                if(o.getKeycode() != c.getKeycode())
                    return false;
                
            default:
        }
        
        
        return true;
    }
    
    
    
    
}
