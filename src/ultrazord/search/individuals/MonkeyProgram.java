/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ultrazord.search.individuals;

import java.io.IOException;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.Random;
import java.util.logging.Level;
import java.util.logging.Logger;
import ultrazord.search.fitness.IndividualVisitor;

/**
 *
 * @author mat
 */
public class MonkeyProgram extends Individual<MonkeyEvent>{

    public MonkeyProgram(){
        super();
    }
    
    @Override
    public Individual getInstance() {
        MonkeyProgram mp = new MonkeyProgram();
        return mp;
    }

    @Override
    public MonkeyEvent getCommandInstance() {
        MonkeyEvent me = new MonkeyEvent();
        return me;
    }

    @Override
    public void accept(IndividualVisitor iv) {
        iv.visit(this);
    }

    @Override
    public void writeToFile(PrintStream out) throws IOException {
        for(MonkeyEvent evt : this){
            out.println(evt);
        }
    }

    

}
