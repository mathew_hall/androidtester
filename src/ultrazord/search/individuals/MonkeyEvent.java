/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ultrazord.search.individuals;

import java.util.ArrayList;
import java.util.Random;
import ultrazord.search.arguments.CoordinateArgument;
import ultrazord.search.arguments.DirectionArgument;
import ultrazord.search.arguments.KeyCodeArgument;
import ultrazord.search.arguments.StringArgument;
import ultrazord.search.arguments.TrackBallArgument;


/**
 *
 * @author mat
 */
public class MonkeyEvent extends Command {
    
    private EventType eventType;
    
    private CoordinateArgument coords;
    private DirectionArgument direction;
    private KeyCodeArgument keycode;
    private StringArgument input;
    private TrackBallArgument trackball;
    
    public MonkeyEvent(){
        coords = new CoordinateArgument();
        direction = new DirectionArgument();
        keycode  = new KeyCodeArgument();
        input = new StringArgument();
        trackball = new TrackBallArgument();
    }
    
    public String toString(){
        StringBuilder b = new StringBuilder(eventType.toString());
        b.append (" ");
        switch(eventType){
            case PRESS:
                b.append(keycode);
                break;
            case TYPE:
                b.append(input);
                break;
            case TRACKBALL:
                b.append(trackball);
                break;
            case TAP:
                b.append(coords);
                break;
            case TOUCH:
                b.append(direction);
                b.append (" ");
                b.append(coords);
                break;
        }
        return b.toString();
    }
    
    public void randomise(Random r){
        coords.randomise(r);
        direction.randomise(r);
        keycode.randomise(r);
        input.randomise(r);
        trackball.randomise(r);
        
        eventType = randomEvent(r);
    }
    
    public MonkeyEvent clone(){
        MonkeyEvent newMe = new MonkeyEvent();
        newMe.coords = coords.clone();
        newMe.direction = direction.clone();
        newMe.keycode = keycode.clone();
        newMe.input = input.clone();
        newMe.trackball = trackball.clone();
        newMe.eventType = eventType;
        
        return newMe;
    }
    
    public void mutate(Random r){
        int num = r.nextInt(3); //either mutate the operator, the operand, or both
        switch(num){
            case 0:
                eventType = randomEvent(r);
            case 1:
                mutateArgument(r);
                break;
            case 2:
                eventType = randomEvent(r);
                break;
        }
    
    }
    
    private void mutateArgument(Random r){
        switch(eventType){
            case PRESS:
                keycode.mutate(r);
                break;
            case TYPE:
                input.mutate(r);
                break;
            case TRACKBALL:
                trackball.mutate(r);
                break;
            case TAP:
                coords.mutate(r);
                break;
            case TOUCH:
                direction.mutate(r);
                coords.mutate(r);
                break;
        }
    }
    
    /**
     * Return a random event type
     * @param r a Random number generator
     * @return a type; where trackballs are 3x more likely to appear than tap, and other events are 2x more likely to appear than tap
     */
    private static EventType randomEvent(Random r){
        //return EventType.values()[r.nextInt(EventType.values().length)];
        ArrayList<EventType> weightedList = new ArrayList<EventType>(){{
            add(EventType.PRESS);
            add(EventType.PRESS);
            add(EventType.TAP);
            add(EventType.TYPE);
            add(EventType.TYPE);
            add(EventType.TRACKBALL);
            add(EventType.TRACKBALL);
            add(EventType.TRACKBALL);
            
        }};
        
        return weightedList.get(r.nextInt(weightedList.size()));
    }

}
enum EventType{
    PRESS,
    TAP,
    TOUCH,
    TYPE,
    TRACKBALL;
    
    public String toString(){
        return this.name().toLowerCase();
        
    }
    
}
