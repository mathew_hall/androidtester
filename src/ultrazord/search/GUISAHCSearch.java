/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ultrazord.search;

/**
 *
 * @author mat
 */
public class GUISAHCSearch extends GUIHCSearch {

    @Override
    /**
     * Evaluate everything before we advance
     */
    protected boolean shouldAdvanceSearch(double parentFitness, double bestFitness, int remainingCommands) {
        return remainingCommands  == 0;
    }
    
}
