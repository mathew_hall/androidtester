/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ultrazord.search;

import ultrazord.search.individuals.MonkeyProgram;
import ultrazord.search.individuals.MonkeyEvent;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashSet;
import java.util.List;
import java.util.Random;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import ultrazord.output.OutputChannels;
import ultrazord.search.individuals.Command;
import ultrazord.search.individuals.Individual;

/**
 *
 * @author mat
 */
public class GASearch<T extends Command> extends Search<T>{
    
    public static void main(String[] args) throws Exception{
        
        
        GASearch search = new GASearch();
        search.runSearch();
        
    }
    private int NUM_GENERATIONS = 150;
    
    

    @Override
    public Result runSearch() throws Exception {
        Random r = new Random();
        
        PrintWriter statusLog = new PrintWriter(OutputChannels.STATUS_LOG);
        ArrayList<Individual<T>> population = new ArrayList<>();
        
        
        statusLog.println("Time,Type,Category,Fitness");
        
        Individual<T> prototype = getPrototype();
        prototype.setLength(Config.configuration.INPUT_LENGTH);
        
        
        
        Logger.getLogger(GASearch.class.getName()).log(Level.INFO, "Initialising population");
        for(int i = 0; i < Config.configuration.GA_POPULATION_SIZE; i++){
            Individual<T> individual = prototype.clone();
            individual.randomise(r);
            population.add(individual);
        }
        
        double bestFitness = -1;
        
        
        for(int i = 0; i < NUM_GENERATIONS; i++){
            Logger.getLogger(GASearch.class.getName()).log(Level.INFO, "Ranking...");
            //Evaluate:
            for(Individual<T> mp : population){
                if(!mp.isEvaluated()){
                    double fitness = fit.getFitness(mp);
                    
                    mp.setFitness(fitness);
                    
                }
                
                logEntry(statusLog, "Individual", "I", mp.getFitness() +"");
            }
            
            Collections.sort(population, Collections.reverseOrder(new Comparator<Individual<?>>() {
                @Override
                public int compare(Individual o1, Individual o2) {
                    return Double.compare(o1.getFitness(), o2.getFitness());
                }
            }));
            

            
            bestFitness = Math.max(bestFitness,population.get(0).getFitness());
            logEntry(statusLog, "Best", "F", bestFitness + "");
            logEntry(statusLog, "Generation", "I", bestFitness+"");
            
            //Select:
            int amount = Math.min(Config.configuration.GA_POPULATION_SIZE, 2 + r.nextInt(Math.max(1 , population.size() - 2)));
            ArrayList<Individual<T>> newParents = new ArrayList<>(population.subList(0, amount));
            
            Logger.getLogger(GASearch.class.getName()).log(Level.INFO, "Selecting {0} individuals", amount);
            //Crossover:
            amount = r.nextInt(Math.min(Config.configuration.GA_CROSSOVER_MAX,(newParents.size() * newParents.size() - 1) /2));
            
            Logger.getLogger(GASearch.class.getName()).log(Level.INFO, "Crossing {0} pairs", amount);
            
            while(amount > 0){
                Individual<T> p1 = newParents.get(r.nextInt(newParents.size()));
                Individual<T> p2 = newParents.get(r.nextInt(newParents.size()));
                
                while(p1 == p2 && newParents.size() > 2){
                    //Avoid mating an individual with itself if possible
                    p2 = newParents.get(r.nextInt(newParents.size()));
                }
                
                newParents.addAll(crossover(p1,p2,r));
                
                
                amount--;
            }
            
            //Mutate:
            amount = Math.min(Config.configuration.GA_MUTATION_MAX, r.nextInt(Math.max(1,newParents.size()) ));
            
            Logger.getLogger(GASearch.class.getName()).log(Level.INFO, "Mutating {0} individuals", amount);
            while(amount > 0){
                Individual<?> target = newParents.get(r.nextInt(newParents.size()));
                target.mutate(r);
                
                amount--;
            }
            
            if(newParents.size() < Config.configuration.GA_POPULATION_SIZE){
                population.removeAll(newParents);
                Logger.getLogger(GASearch.class.getName()).log(Level.INFO, "Refilling new population to go from {0} with {1} individuals from the previous gen", new Object[]{newParents.size(), population.size()});
                while(population.size() > 1 && newParents.size() < Config.configuration.GA_POPULATION_SIZE){
                    newParents.add(population.remove(r.nextInt(population.size())));
                }
            }
            
            if(newParents.size() < Config.configuration.GA_POPULATION_SIZE){
                Logger.getLogger(GASearch.class.getName()).log(Level.INFO, "Adding new individuals to replenish population");
                while(newParents.size() < Config.configuration.GA_POPULATION_SIZE){
                    Individual<T> p = getPrototype();
                    p.randomise(r);
                    newParents.add(p);
                }
            }
            
            population = newParents;
            
        }
        
        
        Result best = null;
        for(Individual<T> iv : population){
            double fitness = 0;
            if(!iv.isEvaluated()){
                fitness = fit.getFitness(iv);
            }else{
                fitness = iv.getFitness();
            }
            
            if(best == null || best.fitness  < fitness){
                best = new Result(fitness, iv);
            }
        }
        return best;
        
    }
    private  ArrayList<Individual<T>> crossover(Individual<T> a, Individual<T> b, Random r){
        Individual<T> newA = a.clone();
        Individual<T> newB = b.clone();
        
        List<T> temp = new ArrayList<>();
        
        int xoverPoint = r.nextInt(Math.min(newA.size(), newB.size()));
        int size = newA.size();
        //Copy the end of A:
        for(int i = xoverPoint; i < size; i++){
            temp.add(newA.get(i));
        }

        //Delete the end of A:
        for(int i = size -1; i >= xoverPoint; i--){
            newA.remove(i);
        }

        //A no longer has the entries at the end of it

        //Copy the tail of B over
        for(int i = xoverPoint; i < size; i++){
            newA.add(b.get(i));
        }

        //delete the tail of B:
        for(int i = size -1; i >= xoverPoint; i--){
            newB.remove(i);
        }

        newB.addAll(temp);
        
        newA.clearEvaluated();
        newB.clearEvaluated();
        
        ArrayList<Individual<T>> ret = new ArrayList<>();
        ret.add(newA);
        ret.add(newB);
        return ret;
    }

    private List<Individual<?>> rouletteSelect(List<Individual<?>> population, int amount, Random r) {
        double totalFitness = 0;
        for(Individual<?> mp : population){
            totalFitness += mp.getFitness();
        }
        
        ArrayList<Individual<?>> newPopulation = new ArrayList<>();
        Set<Individual<?>> selected = new HashSet<>();
        
        
        
        for(int i = 0; i < amount; i++){
            double random = r.nextDouble();    
            double aggregateProportion = 0;
            
            
            Individual<?> individual = null;
            //Walk down the sorted list of individuals
            for(int n = 0; n < population.size(); n++){
                individual = population.get(n);
                
                //get the size of the wheel for this individual
                double proportion = individual.getFitness() / totalFitness;
                aggregateProportion += proportion;
                
                //if the wheel landed here then select the individual (if it hasn't been already).
                if(random < aggregateProportion && !selected.contains(individual))
                    break;
                
                //if the individual was selected, the next one will be selected when the loop restarts.
                
            }
            
            newPopulation.add(individual);
            selected.add(individual);
            
        }
        return newPopulation;
    }

    @Override
    public String getType() {
        return "GA";
    }
    
}
