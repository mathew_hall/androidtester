/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ultrazord.search;

import java.util.Collections;
import java.util.List;
import java.util.Random;
import java.util.logging.Level;
import java.util.logging.Logger;
import ultrazord.search.individuals.RemoteCommand;
import ultrazord.search.individuals.RemoteControlProgram;

/**
 *
 * @author mat
 */
public class GUIRandomSearch extends GUISearch {

    @Override
    protected boolean isFitterThan(double fitness, double bestFitness) {
        return true; //Assume the first random move increases fitness, whether it does or not.
    }

    @Override
    public String getType() {
        return "Random GUI HC";
    }

    @Override
    public int getNumberOfEvaluations() {
        return 1;
    }

    @Override
    public Result runSearch() throws Exception {
        
        Random r = new Random();

        RemoteControlProgram parent = getSuccessor(new RemoteControlProgram(), r, Config.configuration.GUI_COMMAND_SEQUENCE_LENGTH);

        for (int i = 0; fit.getEvaluations() < Config.configuration.EVAL_LIMIT; i++) {

            RemoteControlProgram child = getSuccessor(new RemoteControlProgram(), r, Config.configuration.GUI_COMMAND_SEQUENCE_LENGTH);
            evaluate(parent);
            evaluate(child);
            if (parent.getFitness() < child.getFitness()) {
                parent = child;
                logEntry(statusLog, "Fitness", "F", "" + parent.getFitness());
            } else {
                logEntry(statusLog, "NoChange", "I", "" + child.getFitness());
            }

        }

        double bestFitness = fit.getFitness(parent);
        logEntry(statusLog, "Fitness", "F", "" + bestFitness);
        return new Result(bestFitness, parent);
    }

    @Override
    public RemoteControlProgram getBestCommand(List<RemoteCommand> possibleCommands, RemoteControlProgram parent) throws Exception {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
}
