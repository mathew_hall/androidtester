/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ultrazord.search;

import java.io.IOException;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.logging.Level;
import java.util.logging.Logger;
import main.RemoteCommandFailureException;
import org.apache.commons.math3.distribution.PoissonDistribution;
import org.apache.commons.math3.random.JDKRandomGenerator;
import org.apache.commons.math3.random.RandomAdaptor;
import org.apache.commons.math3.random.RandomGenerator;
import ultrazord.search.individuals.RemoteCommand;
import ultrazord.search.individuals.RemoteControlProgram;

/**
 *
 * @author mat
 */
public class GUIFullTraceSA extends GUISearch {
    
    PoissonDistribution dist;
    double temperature = 1.0;
    
    public GUIFullTraceSA(){
        
    }
    
    @Override
    public String getType() {
        return "Random GUI SA";
    }

    @Override
    public int getNumberOfEvaluations() {
        return 1;
    }
    


    @Override
    public Result runSearch() throws Exception {
        RemoteControlProgram parent = new RemoteControlProgram();
        parent.setLength(30);
        RandomGenerator rg= new JDKRandomGenerator();
        RandomAdaptor r= new RandomAdaptor(rg);
        parent = getSuccessor(parent, r, Config.configuration.GUI_COMMAND_SEQUENCE_LENGTH);
        
        double dt = 1.0 / Config.configuration.EVAL_LIMIT;
        
        for (int i = 0; fit.getEvaluations() < Config.configuration.EVAL_LIMIT; i++) {
            RemoteControlProgram child;
            
            
            evaluate(parent);

            int point;
            
            //Poisson distribution with mean = size * temp - as temp decreases, mean shifts towards 0
            PoissonDistribution pd = new PoissonDistribution(rg, 1 + (temperature * (Config.configuration.GUI_COMMAND_SEQUENCE_LENGTH - 1)),PoissonDistribution.DEFAULT_EPSILON, PoissonDistribution.DEFAULT_MAX_ITERATIONS);
            
            //length - sample flips the distribution so when mean = 0 in poisson, mean is 15 in inverse.
            //when mean is 15 in poisson, mean is 0 in inverse.
            point = (Config.configuration.GUI_COMMAND_SEQUENCE_LENGTH - 1) - pd.sample();
            
            //chop off negative tail
            point = Math.abs(point);
            
            if(point >= Config.configuration.GUI_COMMAND_SEQUENCE_LENGTH){
                Logger.getLogger(GUIFullTraceSA.class.getName()).log(Level.SEVERE, "Off-by-one in Poission code!");
                point = Config.configuration.GUI_COMMAND_SEQUENCE_LENGTH;
            }
            
            child = parent.clone();
            
            
            
            //delete the tail:
            while(child.size() > point){
                child.remove(point);
            }
            
            Logger.getLogger(GUIFullTraceSA.class.getName()).log(Level.INFO, "Set cutpoint to {0}. Child now has {1} elements", new Object[]{point, child.size()});
            
            child = getSuccessor(child, r, Config.configuration.GUI_COMMAND_SEQUENCE_LENGTH);
            
            
            
            evaluate(child);
            
            logEntry(statusLog,"Fitness", "I", ""+child.getFitness());
            
            if(isFitterThan(child.getFitness(), parent.getFitness())){
                parent = child;
                logEntry(statusLog,"Change", "F", ""+ parent.getFitness());
            }else{
                logEntry(statusLog,"NoChange", "I", "" + child.getFitness());
            }
            
            temperature = 1.0 - (dt * fit.getEvaluations());
            if(temperature <0){
                Logger.getLogger(GUIFullTraceSA.class.getName()).log(Level.SEVERE, "Temperature < 0! temp: {0} dt: {1}", new Object[]{temperature, dt});
                temperature = 0;
            }
        }

        double bestFitness = fit.getFitness(parent);
        logEntry(statusLog, "Fitness", "F", "" + bestFitness);
        return new Result(bestFitness, parent);
    }

    @Override
    public RemoteControlProgram getBestCommand(List<RemoteCommand> possibleCommands, RemoteControlProgram parent) throws Exception {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    

}
