/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ultrazord.search.converters;

import com.beust.jcommander.IStringConverter;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author mat
 */
public class FileStreamConverter implements IStringConverter<FileOutputStream> {

    @Override
    public FileOutputStream convert(String string) {
        try {
            FileOutputStream fs = new FileOutputStream(string);
            return fs;
        } catch (FileNotFoundException ex) {
            Logger.getLogger(ultrazord.search.converters.FileStreamConverter.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
    }
}
