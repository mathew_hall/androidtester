/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ultrazord.search.converters;

import com.beust.jcommander.IStringConverter;
import java.util.logging.Level;
import java.util.logging.Logger;
import ultrazord.search.fitness.FitnessEvaluator;

/**
 *
 * @author mat
 */
public class FitnessFunctionClassConverter implements IStringConverter<Class<? extends FitnessEvaluator>> {

    @Override
    public Class convert(String string) {
        try {
            return (Class<? extends FitnessEvaluator>) Class.forName(string);
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(FitnessFunctionClassConverter.class.getName()).log(Level.SEVERE, null, ex);
            System.exit(0);
        }
        return null;
    }
    
}
