/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ultrazord.search.converters;

import com.beust.jcommander.IStringConverter;
import java.util.logging.Level;
import java.util.logging.Logger;
import ultrazord.search.Search;

/**
 *
 * @author mat
 */
public class SearchClassConverter implements IStringConverter<Class<? extends Search>> {

    @Override
    public Class convert(String string) {
        try {
            return (Class<? extends Search>) Class.forName(string);
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(SearchClassConverter.class.getName()).log(Level.SEVERE, null, ex);
            System.exit(0);
        }
        return null;
    }
    
}
