/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ultrazord.search;

import com.example.batteryburner.tests.Commands;
import ultrazord.search.individuals.RemoteCommand;
import ultrazord.search.individuals.RemoteControlProgram;

import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author mat
 * Base class for all hill climbing style searches.
 * At each step, it will get the commands and ask the implementing class if it's better,
 * if it is, then it will use that as the current best individual.
 * It will then ask shouldAdvanceSearch is this iteration is done via shouldAdvanceSearch
 */
public abstract class GUIHCSearch extends GUISearch {



    protected abstract boolean shouldAdvanceSearch(double parentFitness, double bestFitness, int remainingCommands);
    
    protected void onNextIteration(){ }

    @Override
    public RemoteControlProgram getBestCommand(List<RemoteCommand> commands, RemoteControlProgram parent) throws Exception {
        double parentFitness = parent.getFitness();
        double bestFitness = parentFitness;
        RemoteControlProgram best = parent;
        int i = 0;
        
        onNextIteration();
        
        for (RemoteCommand rc : commands) {
            /**
             * We don't waste time just typing text. If we see a text event we
             * silently accept it and replay it with all subsequent candidate
             * events.
             */
            if (rc.getCommand().getType() == Commands.UIInteraction.Type.SENDKEY) {
                parent.add(rc);
                continue;
            }
            RemoteControlProgram p = parent.clone();
            Logger.getLogger(GUIHCSearch.class.getName()).log(Level.INFO, "RC: {0}", rc);
            p.add(rc);
            double fitness = evaluate(p);


            if (isFitterThan(fitness, bestFitness)) {
                bestFitness = fitness;
                best = p;
                Logger.getLogger(GUIHCSearch.class.getName()).log(Level.INFO, "Taking change");
                logEntry(statusLog, "Change", "I", "" + fitness);
            } else {
                logEntry(statusLog, "Fitness", "I", "" + fitness);
            }
            if (shouldAdvanceSearch(parentFitness, bestFitness, commands.size() - 1 - i)) {
                break;
            }

            i++;
        }
        return best;
    }
}
