/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ultrazord.search;

import ultrazord.search.converters.SearchClassConverter;
import ultrazord.search.converters.FitnessFunctionClassConverter;
import ultrazord.search.converters.FileStreamConverter;
import com.beust.jcommander.Parameter;
import com.beust.jcommander.converters.FileConverter;
import java.io.File;
import java.io.FileOutputStream;
import java.util.ArrayList;
import java.util.List;
import ultrazord.search.fitness.FitnessEvaluator;
import ultrazord.search.fitness.UtimeFitnessFunction;
import ultrazord.search.individuals.Individual;

/**
 *
 * @author mat
 *
 * CLI args can be put in a file, one per line. Use
 * @/path/to/file as the argument See http://jcommander.org/#Syntax for more
 * details
 *
 */
public class Config {

    @Parameter(names = "-screen_width")
    public int SCREEN_WIDTH = 600;
    @Parameter(names = "-screen_height")
    public int SCREEN_HEIGHT = 1280;
    @Parameter(names = "-string_length")
    public int STRING_LENGTH = 10;
    @Parameter(names = "-num_events")
    public int NUM_EVENTS = 30;
    @Parameter(names = "-throttle_events", arity = 1)
    public boolean THROTTLE_EVENTS = true;
    @Parameter(names = "-throttle_delay")
    public int THROTTLE_DELAY = 200;
    @Parameter(names = "-max_keycode")
    public int MAX_KEYCODE = 210;
    @Parameter(names = "-forbidden_keycodes")
    public List<Integer> FORBIDDEN_KEYCODES = new ArrayList<Integer>();
    @Parameter(names = "-monkey_port")
    public int MONKEY_PORT = 1080;
    @Parameter(names = "-app_uid")
    public int APP_UID;
    @Parameter(names = "-preflight")
    public String PREFLIGHT_SCRIPT;
    @Parameter(names = "-postflight")
    public String POSTFLIGHT_SCRIPT;
    @Parameter(names = "-debug_monkey")
    public boolean DEBUG_MONKEY = false;
    @Parameter(names = " -app_kill_timeout")
    public long APP_KILL_TIMEOUT = 30_000;
    @Parameter(names = "-monitoring_data_dir", converter = FileConverter.class)
    public File MONITORING_DATA_DIR;
    public static Config configuration;
    @Parameter(names = "-input_length")
    public int INPUT_LENGTH = 30;
    @Parameter(names = "-ga_population_size")
    public int GA_POPULATION_SIZE = 10;
    @Parameter(names = "-ga_crossover_max")
    public int GA_CROSSOVER_MAX = 10;
    @Parameter(names = "-ga_mutation_max")
    public int GA_MUTATION_MAX = 0;
    @Parameter(names = "-search_fitness_function", converter = FitnessFunctionClassConverter.class)
    public Class<? extends FitnessEvaluator> SEARCH_FITNESS_FUNCTION = UtimeFitnessFunction.class;
    public Class<? extends Individual> SEARCH_INDIVIDUAL;
    @Parameter(names = "-search_type", converter = SearchClassConverter.class)
    public Class<? extends Search> SEARCH_TYPE = GUIHCSearch.class;
    @Parameter(names = "-remote_button_delay")
    public int REMOTE_BUTTON_DELAY = 5000; //max 150s per test.
    @Parameter(names = "-adb")
    public String ADB = "/Users/mat/Library/AVD/sdk/platform-tools/adb";
    @Parameter(names = "-gui_command_sequence_length")
    public int GUI_COMMAND_SEQUENCE_LENGTH = 20;
    @Parameter(names = "-wait_for_debug")
    public boolean WAIT_FOR_DEBUG = false;
    @Parameter(names = "-eval_limit")
    public int EVAL_LIMIT = 50;
    @Parameter(names = "-individual")
    public String INDIVIDUAL = "";
    @Parameter(names = "-trace_log_path", converter = FileStreamConverter.class)
    public FileOutputStream TRACE_LOG_PATH;

    public Config() {
        configuration = this;
    }

    @Override
    public String toString() {
        return "SCREEN_WIDTH=" + SCREEN_WIDTH + "\nSCREEN_HEIGHT=" + SCREEN_HEIGHT + "\nSTRING_LENGTH=" + STRING_LENGTH + "\nNUM_EVENTS=" + NUM_EVENTS + "\nTHROTTLE_EVENTS=" + THROTTLE_EVENTS + "\nTHROTTLE_DELAY=" + THROTTLE_DELAY + "\nMAX_KEYCODE=" + MAX_KEYCODE + "\nFORBIDDEN_KEYCODES=" + FORBIDDEN_KEYCODES + "\nMONKEY_PORT=" + MONKEY_PORT + "\nAPP_UID=" + APP_UID + "\nPREFLIGHT_SCRIPT=" + PREFLIGHT_SCRIPT + "\nPOSTFLIGHT_SCRIPT=" + POSTFLIGHT_SCRIPT + "\nDEBUG_MONKEY=" + DEBUG_MONKEY + "\nAPP_KILL_TIMEOUT=" + APP_KILL_TIMEOUT + "\nMONITORING_DATA_DIR=" + MONITORING_DATA_DIR + "\nINPUT_LENGTH=" + INPUT_LENGTH + "\nGA_POPULATION_SIZE=" + GA_POPULATION_SIZE + "\nGA_CROSSOVER_MAX=" + GA_CROSSOVER_MAX + "\nGA_MUTATION_MAX=" + GA_MUTATION_MAX + "\nSEARCH_FITNESS_FUNCTION=" + SEARCH_FITNESS_FUNCTION;
    }
}
