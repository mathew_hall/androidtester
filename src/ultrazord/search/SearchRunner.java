/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ultrazord.search;

import com.beust.jcommander.JCommander;
import java.io.File;
import java.io.PrintStream;
import java.io.PrintWriter;
import java.util.logging.Level;
import java.util.logging.Logger;
import ultrazord.search.Search.Result;

/**
 *
 * @author mat
 */
public class SearchRunner {
    
    public static void init(String[] args){
        Config c = new Config();
        new JCommander(c, args);
    }
    
    public static void main(String[] args) throws InstantiationException, IllegalAccessException, Exception{
        
        init(args);
        
        Search s = Config.configuration.SEARCH_TYPE.newInstance();
        
        Result res = s.runSearch();
        Logger.getLogger(SearchRunner.class.getName()).log(Level.INFO, "Search finished");
        
        File outFile = File.createTempFile("best", "individual");
        PrintStream ps = new PrintStream(outFile);
        PrintWriter pw = new PrintWriter(ps);
        
        pw.println(res.fitness);
        pw.flush();
        res.individual.writeToFile(ps);
        ps.flush();
        ps.close();
        
        Logger.getLogger(SearchRunner.class.getName()).log(Level.INFO, "Written to {0}", outFile);
        
        
        
        
    }
    
    
}
