/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ultrazord.search.fitness;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.UnknownHostException;
import java.util.logging.Level;
import java.util.logging.Logger;
import ultrazord.search.Config;

/**
 *
 * @author mat
 */
public class UtimeFitnessFunction extends FitnessEvaluator{
    
    private static final String PACKAGE_NAME = "org.wordpress.android"; //TODO: break this out into a configuration entry.
    
    private String before;
    private String after;
    private int pid;

    private PrintWriter statLog = null;
    
    public UtimeFitnessFunction() throws UnknownHostException, IOException {
        super();
        try {
            statLog = new PrintWriter("/tmp/statlog.log");
            
        } catch (FileNotFoundException ex) {
            Logger.getLogger(UtimeFitnessFunction.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    @Override
    protected void beforeTest() throws Exception {
        String[] cmd = new String[]{
            Config.configuration.ADB,
            "shell",
            "ps | grep " + PACKAGE_NAME
        };
        Process p = Runtime.getRuntime().exec(cmd);
        
        InputStream is = p.getInputStream();
        BufferedReader br = new BufferedReader(new InputStreamReader(is));
        String in;
        String pidString = null;
        while((in = br.readLine()) != null && !in.startsWith("system")){ //sometimes system seems to run something that looks like the target; ignore it.
            pidString=in.split("\\s+")[1];
        }
        is.close();
        if(pidString == null){
            Logger.getLogger(UtimeFitnessFunction.class.getName()).log(Level.INFO, "Coouldn't get PID");
            
        }
        pid = Integer.parseInt(pidString);
        
        Logger.getLogger(UtimeFitnessFunction.class.getName()).log(Level.INFO, "PID of app is {0}", pidString);
        before = readProcStat(pid);
        
        
    }

    @Override
    protected void afterTest() throws Exception {
        after = readProcStat(pid);
        pid = -1;
    }

    
    
    @Override
    protected double getFitness() throws Exception {
        String utimeBefore = before.split("\\s+")[13];
        String utimeAfter  = after.split("\\s+")[13];
        
        int beforeInt = Integer.parseInt(utimeBefore);
        int afterInt = Integer.parseInt(utimeAfter);
        
        return afterInt - beforeInt;
        
        
    }

    public String readProcStat(int pid) throws IOException {
        String[] cmd;
        Process p;
        InputStream is;
        BufferedReader br;
        String in;
        cmd = new String[]{
            Config.configuration.ADB,
            "shell",
            "cat /proc/" + pid + "/stat"
        };
        p = Runtime.getRuntime().exec(cmd);
        
        is = p.getInputStream();
        br = new BufferedReader(new InputStreamReader(is));
        String statLine = null;
        while((in=br.readLine()) != null){
            statLine = in;
        }
        statLog.println(statLine);
        statLog.flush();
        
        return statLine;
    }
    
    
    
    
}
