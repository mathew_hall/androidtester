/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ultrazord.search.fitness;

import ultrazord.search.individuals.MonkeyProgram;
import ultrazord.search.individuals.RemoteControlProgram;

/**
 *
 * @author mat
 */
public interface IndividualVisitor {
    public void visit(MonkeyProgram mp);
        
    
    public void visit(RemoteControlProgram rp);
        
}
