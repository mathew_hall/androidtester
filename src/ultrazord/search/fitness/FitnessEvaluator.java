/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ultrazord.search.fitness;

import main.RemoteApp;
import main.RemoteCommandFailureException;
import ultrazord.output.MultiplePrintWriter;
import ultrazord.output.OutputChannels;
import ultrazord.search.AndroidDevice;
import ultrazord.search.Config;
import ultrazord.search.individuals.*;

import java.io.*;
import java.net.Socket;
import java.net.UnknownHostException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author mat
 */
public abstract class FitnessEvaluator implements IndividualVisitor {

    Socket clientSocket = null;
    Writer monkeyPipe;
    private static final int MAX_RETRIES = 5;
    private int evaluations = 0;
    
    int num = 0;
    
    /**
     * Repeat a test this many times and take the average. This sample size was
     * gathered based on the maximum distance from a large sample mean for
     * subsamples with the utime fitness function. The test in question relied
     * on loading web pages so the performance of the webserver may have
     * introduced variability. If that's the case, then the actual accuracy
     * might be higher than this one.
     */
    private static final int SAMPLE_SIZE = 2;
    private File screenshotPath;
    private boolean monkeyPipeActive = false;

    public FitnessEvaluator() throws UnknownHostException, IOException {
        screenshotPath = new File("/tmp/screenshots/");
        screenshotPath.mkdirs();

    }

    public int getEvaluations() {
        return evaluations;
    }

    public void setEvaluations(int evals) {
        evaluations = evals;
    }

    /**
     * Called after a test has run. It's up to the implementing class to decide
     * how to pull values from the device.
     *
     * @return
     */
    protected abstract double getFitness() throws Exception;

    public double getFitness(Individual script) throws Exception {
        int tries = 0;
        int n = 0;
        
        double samples[] = new double[SAMPLE_SIZE];
        
        double sum = 0;
        while (tries < MAX_RETRIES && n < SAMPLE_SIZE) {
            try {
                double fitness = runTest(script);
                sum += fitness;
                samples[n] = fitness;
                
                n++;
            } catch (FitnessValueErrorException fe) {
                tries++;
            }
        }
        
        for(double x : samples){
            for(double y : samples){
                double amount;
                if( x > y){
                    amount = x / y;
                }else{
                    amount = y/x;
                }
                if(amount >= 1.1){
                    Logger.getLogger(FitnessEvaluator.class.getName()).log(Level.INFO, "Warning; two runs {0}, {1} are more than 10% apart!", new Object[]{x,y});
                }
            }
        }

        evaluations++;
        return sum / n;

    }

    protected double runTest(Individual script) throws Exception {
        AndroidDevice.preflight();
        //App is now ready to go
        beforeTest();
        try {
            //wait for monitoring to start recording before we spam the app:
            Thread.sleep(5000);
        } catch (Exception ignored) {
        }

        if(!screenshotPath.exists()){
            screenshotPath.mkdirs();
        }
        script.accept(this);
        
        


        afterTest();

        AndroidDevice.postflight();
        if (clientSocket != null && !clientSocket.isInputShutdown()) {
            clientSocket.shutdownInput(); //ignore any "OK" messages monkey might have sent.
        }


        double fitness = getFitness();
        PrintWriter pw = new PrintWriter(OutputChannels.FITNESS_LOG);
        pw.append(fitness + "");
        pw.append("\n");
        pw.flush();
        
        Path target = screenshotPath.toPath().resolveSibling("screenshot_" + num);
        if(target.toFile().exists()){
            target.toFile().renameTo(new File(System.currentTimeMillis() + ""));
        }
        
        Files.move(screenshotPath.toPath(), target);
        
        File fitnessFile = target.resolve("fitness.txt").toFile();
        PrintWriter pwf = new PrintWriter(fitnessFile);
        pwf.println(fitness);
        pwf.flush();
        pwf.close();
                        
        num++;
        
        return fitness;
    }

    /**
     * Called after the app is started but before the script runs.
     *
     * @throws Exception
     */
    protected void beforeTest() throws Exception {
        //Noop - optional for implementing classes
    }

    /**
     * Called after the test script finishes (and the timeout if any).
     *
     * @throws Exception
     */
    protected void afterTest() throws Exception {
        //Noop - optional for implementing classes.
    }

    @Override
    public void visit(MonkeyProgram mp) {
        int i = 0;
        //Just in case, initialise Monkey:
        setupMonkeyPipe();

        takeScreenshot(screenshotPath, i);
        try {
            monkeyPipe.append("###\n");
            for (MonkeyEvent e : mp) {
                monkeyPipe.append(e.toString());
                monkeyPipe.append("\n");
                try {
                    Thread.sleep(Config.configuration.REMOTE_BUTTON_DELAY);
                } catch (InterruptedException ex) {
                    Logger.getLogger(FitnessEvaluator.class.getName()).log(Level.SEVERE, null, ex);
                }
                takeScreenshot(screenshotPath, i);
                i++;
            }
            monkeyPipe.flush();
            try {
                Thread.sleep(Config.configuration.APP_KILL_TIMEOUT);
            } catch (Exception e) {
            }
        } catch (IOException ex) {
            Logger.getLogger(FitnessEvaluator.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    private void takeScreenshot(File directory, int number) {
        if (directory == null) {
            return;
        }
        String[] cmd = new String[]{
            Config.configuration.ADB,
            "shell",
            "screencap -p /sdcard/shot.png"
        };
        try {
            Process p = Runtime.getRuntime().exec(cmd);
            p.waitFor();

            cmd = new String[]{
                Config.configuration.ADB,
                "pull",
                "/sdcard/shot.png",
                "/tmp/shot.png"
            };
            p = Runtime.getRuntime().exec(cmd);
            p.waitFor();

            File screenshot = new File("/tmp/shot.png");
            File target = directory.toPath().resolve("screenshot_" + number + ".png").toFile();
            if(target.exists()){
                target.delete();
            }
            Files.move(screenshot.toPath(), target.toPath());

        } catch (IOException ex) {
            Logger.getLogger(FitnessEvaluator.class.getName()).log(Level.SEVERE, null, ex);
        } catch (InterruptedException ex) {
            Logger.getLogger(FitnessEvaluator.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Override
    public void visit(RemoteControlProgram rp) {
        RemoteApp app = new RemoteApp();
        
        PrintWriter out = new PrintWriter(OutputChannels.TRACE_DATA, true);
        int i = 0;

        i++;
        
        takeScreenshot(screenshotPath, i);
        out.println("trace");
        for (RemoteCommand c : rp) {
            try {
                
                
                String oldActivity = app.getCurrentActivity().split("=")[1];
                
                app.sendCommand(c.getCommand().toBuilder());
                Thread.sleep(Config.configuration.REMOTE_BUTTON_DELAY);
                
                String newActivity = app.getCurrentActivity().split("=")[1];
                
                takeScreenshot(screenshotPath, i);
                
                switch(c.getCommand().getType()){
                    case PRESSVIEW:
                        out.printf("pressview %d %s %s\n", 
//                            c.getCommand().getType().toString().toLowerCase(),
                            c.getCommand().getId(),
                            oldActivity,
                            newActivity
                            );
                        break;
                    case SENDKEY:
                        if(c.getCommand().getId() == 4){
                            out.println("backbutton");
                        }
                        break;
                    case TEXT:
                        out.printf("text %d %s %s %s\n",
                            c.getCommand().getId(),
                            c.getCommand().getText(),
                            oldActivity,
                            newActivity
                            );
                        
        
                }
                
            } catch (RemoteCommandFailureException | IOException | InterruptedException ex) {
                Logger.getLogger(FitnessEvaluator.class.getName()).log(Level.SEVERE, null, ex);
            }
            i++;
        }
        try {
            Thread.sleep(3000); //wait a little bit for the last event to be processed.
        } catch (Exception e) {
        }
        takeScreenshot(screenshotPath, i);
    }

    protected void setupMonkeyPipe() {
        if(monkeyPipeActive)return;
        monkeyPipeActive = true;
        if (Config.configuration.DEBUG_MONKEY) {
            monkeyPipe = new PrintWriter(System.out);
        } else {
            try {
                clientSocket = new Socket("localhost", Config.configuration.MONKEY_PORT);
                OutputStream s = clientSocket.getOutputStream();

                monkeyPipe = new MultiplePrintWriter(new OutputStream[]{s, OutputChannels.INDIVIDUAL_OUTPUT});
            } catch (Exception ex) {
                Logger.getLogger(FitnessEvaluator.class.getName()).log(Level.SEVERE, null, ex);
                throw new RuntimeException("IO Exception in Monkey initialisation; is the port forwarded?");
            } 
        }
    }
}
