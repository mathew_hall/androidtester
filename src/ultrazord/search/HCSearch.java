/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ultrazord.search;

import ultrazord.output.OutputChannels;
import ultrazord.search.individuals.Individual;
import ultrazord.search.individuals.MonkeyProgram;

import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author mat
 */
public abstract class HCSearch extends Search{


    @Override
    public Result runSearch() throws Exception {
        Random r = new Random();

        Individual mp = new MonkeyProgram();
        
        mp.setLength(Config.configuration.INPUT_LENGTH);
        mp.randomise(r);
        
        PrintWriter statusLog = new PrintWriter(OutputChannels.STATUS_LOG);
        
        statusLog.println("Time,Type,Category,Fitness");
        
        double fitness = fit.getFitness(mp);

        logEntry(statusLog, "Start","F", fitness +"");
        Logger.getLogger(HCSearch.class.getName()).log(Level.INFO, "Fitness of start program: {0}", fitness);
        int noIncrease = 0;
        
        List<Result> best = new ArrayList<Result>();
        
        for(int i = 0; i < 100; i++){
            Individual mutant = mp.clone();
            mutant.mutate(r);
            
            double mutantF = fit.getFitness(mutant);
            if(mutantF > fitness){
                Logger.getLogger(HCSearch.class.getName()).log(Level.INFO, "Increase in fitness from {0} to {1}", new Object[]{(Object)new Double(fitness), (Object)new Double(mutantF)});
                mp = mutant;
                fitness = mutantF;
                logEntry(statusLog, "Change","F", fitness +"");
                noIncrease = 0;
            }else{
                Logger.getLogger(HCSearch.class.getName()).log(Level.INFO, "Mutant didnt increase fitness from {0};  {1}. Mutants are {2} and {3} entries", new Object[]{(Object)new Double(fitness), (Object)new Double(mutantF), mp.size(), mutant.size()});
                logEntry(statusLog, "NoChange","I", mutantF +"");
                logEntry(statusLog, "Same", "F", fitness+ "");
                noIncrease++;
            }
            
            if(shouldRestart(noIncrease, r)){
                best.add(new Result(fitness,mp.clone()));
                mp.randomise(r);
                fitness = fit.getFitness(mp);
                logEntry(statusLog, "Restart", "F", fitness+"");
                noIncrease = 0;
                
            }
            if(i%10 == 0){
                Logger.getLogger(HCSearch.class.getName()).log(Level.INFO, "Done {0} iterations of 100", i);
            }
        }
        
        best.add(new Result(fitness,mp));
        
        Logger.getLogger(HCSearch.class.getName()).log(Level.INFO, "Finished");
        
        Result bestResult = best.get(0);
        
        for(Result m : best){
            if(m.fitness > bestResult.fitness){
                bestResult = m;
            }
        }
        
        return bestResult;
    }
    

    protected abstract boolean shouldRestart(int noIncrease, Random r);
        
}

